@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Listado de Mesas <a href="" data-target="#modal-create-category" data-toggle="modal"><button class="btn btn-default"><i class="fa fa-plus-circle text-green"></i> Nuevo</button></a></h3>
    </div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>Numero</th>
                    <th>Imagen</th>
                    <th>Status</th>
                    <th>Opciones</th>
                </thead>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('item.mesa.create')
@push ('scripts')
  <script>
  $(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    $('#table_id').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'ocurrio un error', location.reload() );
    } ).DataTable( {
      "order": [[ 0, 'desc' ]],
      "processing": false,
      "infoFiltered":false,
      "ordering":true,
      "paging":true,
      "stateSave": true,
      "stateDuration": -1,
        "serverSide": true,
        "ajax": "/api/mesas",
        "columns":[
          {data:'numero',searchable: false},
          {data:'image'},
          {data:'status'},
          {data:'btn',searchable: false,"orderable": false},
        ],

        createdRow: (row, data, dataIndex, cells) => {
			if(data['status']=='Disponible')
            {
        		$(cells).css({"background-color":"#4CCA00"})
			}
            else if(data['status']=='Ocupada')
            {
        		$(cells).css({"background-color":"#D87F74"})
			}
            else
            {
                $(cells).css({"background-color":"#EBEE00"})
            }
		},

        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        }

    } );
} );
</script>
@endpush
@endsection
