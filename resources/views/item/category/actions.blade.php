<a href="" data-target="#modal-edit-{{$id_category}}" onclick="abrir_edit({{$id_category}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('item.category.edit')
<a href="" data-target="#modal-delete-{{$id_category}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('item.category.modal')
