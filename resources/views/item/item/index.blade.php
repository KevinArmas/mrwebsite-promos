@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Listado de Articulos y Productos<a href="" data-target="#modal-create-item" data-toggle="modal"><button class="btn btn-default"><i class="fa fa-plus-circle text-green"></i> Nuevo</button></a></h3>
    </div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Precio</th>
                    <th>Imagen</th>
                    <th>Observaciones</th>
                    <th>Opciones</th>
                </thead>

            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('item.item.create')
@push ('scripts')
  <script>
  $(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    $('#table_id').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'ocurrio un error', location.reload() );
    } ).DataTable( {
      "order": [[ 0, 'desc' ]],
      "processing": false,
      "infoFiltered":false,
      "ordering":true,
      "paging":true,
      "stateSave": true,
      "stateDuration": -1,
        "serverSide": true,
        "ajax": "/api/items",
        "columns":[
          {data:'id_item',searchable: false},
          {data:'name'},
          {data:'category', name:'category.name'},
          {data:'price'},
          {
            'data': 'img',
            'className': "text-center", 
            'sortable': false,
            'searchable': false,
            'render': function (img) {
                if (!img) {
                    return '';
                }
                else {
                    var imge = '/img/productos/' + img;
                    return '<img src="' + imge + '" height="130px" />';
                }
            }
          },
          {data:'observation'},
          {data:'btn',searchable: false,"orderable": false},
        ],
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        }

    } );
} );

function abrir_edit(i)
{
  var brand=$('#id_category_edit_'+i).val();
	var data = {!!  json_encode($category) !!};
	var prov ='<option value= "">- Seleccione -</option>';
	for (var a=0; a<data.length; ++a)
	{
    if(brand==data[a].id_category)
    {
      prov += '<option selected value= "'+data[a].id_category+'">'+data[a].name+'</option>';
    }
    else{
      prov += '<option value= "'+data[a].id_category+'">'+data[a].name+'</option>';
    }
	}
	$('#id_category_edit_'+i).html(prov);

	$('#id_category_edit_'+i).select2({
		dropdownAutoWidth: true,
		minimumInputLength: 1,
        dropdownParent: $('#modal-edit-'+i),
  });

  
}
</script>
@endpush
@endsection
