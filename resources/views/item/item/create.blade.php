<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-create-item" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Agregar Artículo o Servicio</h4>
			</div>

	{!!Form::open(array('url'=>'item/item','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'files'=>'true', 'onsubmit'=>'return checkSubmit();'))!!}
<div class="modal-body" style="min-height:300px; ">

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Nombre</label>
			<input type="text" class="form-control" required name="name" placeholder="Nombre..." value="{{ old('name') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Categoría</label>
			<select name="id_category" class="form-control" required>
				@foreach($category as $c)
				<option value="{{$c->id_category}}">{{$c->name}}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Precio</label>
			<input type="number" step="0.01" class="form-control" required name="price" placeholder="Precio..." value="{{ old('price') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Observación</label>
			<textarea class="form-control" placeholder="Observación..." name="observation">{{ old('observation') }}</textarea>
		</div>
	</div>

	<div class="col=lg-6 col-sm-6 col-md-6 col-xs-12">
		<div class="form-group">
			<label>Imagen</label>
			<input type="file" name="image" class="form-control" placeholder="Imagen del artículo...">
		</div>
	</div>
</div>

<div class="modal-footer">
	<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
</div>
{!!Form::close()!!}
</div>
</div>
</div>
@push('scripts')
<script>
	enviando = false; //Obligaremos a entrar el if en el primer submit

	function checkSubmit() {
		if (!enviando) {
		enviando= true;
		return true;
		}
		else {
		//Si llega hasta aca significa que pulsaron 2 veces el boton submit
		alert("El formulario ya se esta enviando");
		return false;
		}
	}
</script>
@endpush
