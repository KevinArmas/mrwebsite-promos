<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$id_item}}" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Editar Artículo o Servicio</h4>
			</div>
		{!!Form::open(array('url'=>'editar/item','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'files'=>'true', 'onsubmit'=>'return checkSubmit();'))!!}
		<input type="hidden" name="id" value="{{ $id_item}}">
<div class="modal-body" style="min-height:300px; ">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Nombre</label>
			<input type="text" class="form-control" required name="name" placeholder="Nombre..." value="{{$name}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Precio</label>
			<input type="number" step="0.01" class="form-control" required name="price" placeholder="Precio..." value="{{$price}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Categoría</label><br>
			<select name="id_category" id="id_category_edit_{{$id_item}}" class="form-control select2" style="width: 100%" required>
					<option value="{{$id_category}}">{{$category}}</option>
			</select>
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Observación</label>
			<textarea class="form-control" placeholder="Observación..." name="observation">{{$observation}}</textarea>
		</div>
	</div>

	<div class="col=lg-6 col-sm-6 col-md-6 col-xs-12">
		<div class="form-group">
			<label>Imagen</label>
			<input type="file" name="image" class="form-control" placeholder="Imagen del artículo...">
			@if (($img)!="")
				<img src="{{asset('/img/productos/'.$img)}}" heigth="200px" width="200px">
			@endif
		</div>
	</div>

</div>
<div class="modal-footer">
	<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
</div>
{{Form::Close()}}
</div>
</div>
</div>
@push('scripts')
<script>
enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
	if (!enviando) {
	enviando= true;
	return true;
	}
	else {
	//Si llega hasta aca significa que pulsaron 2 veces el boton submit
	alert("El formulario ya se esta enviando");
	return false;
	}
}
</script>
@endpush
