<a href="" data-target="#modal-edit-{{$id_item}}" onclick="abrir_edit({{$id_item}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('item.item.edit')
<a href="" data-target="#modal-delete-{{$id_item}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('item.item.modal')
