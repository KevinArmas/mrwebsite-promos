<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$id_rol}}" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Editar Rol</h4>
			</div>
			{!!Form::open(array('url'=>'editar/rol','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'onsubmit'=>'return checkSubmit();'))!!}
			<div class="modal-body" style="min-height:300px; ">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<input type="hidden" name="id" value="{{ $id_rol}}">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" required placeholder="Nombre..." name="name" value="{{$name}}">
						</div>
					</div>

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Descripción</label>
							<textarea class="form-control" placeholder="Descripción..." name="description">{{$description}}</textarea>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Permisos en Módulos</label>
							<div class="table-responsive" style="height: 250px; overflow-y: scroll;">
								<table class="table table-striped table-bordered table-condensed table-hover" width="100%">
									<thead>
										<th class="bg-info text-center" colspan="2" width="85%">Descripción</th>
										<th class="bg-info text-center">Permiso</th>
									</thead>
									<tbody id="lista_permisos_edit{{$id_rol}}">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
			</div>
			{{Form::Close()}}
		</div>
	</div>
</div>
@push('scripts')
<script>
enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
	if (!enviando) {
	enviando= true;
	return true;
	}
	else {
	//Si llega hasta aca significa que pulsaron 2 veces el boton submit
	alert("El formulario ya se esta enviando");
	return false;
	}
}
</script>
@endpush
