<a href="" data-target="#modal-edit-{{$id_rol}}" onclick="abrir_edit({{$id_rol}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('security.rol.edit')
<a href="" data-target="#modal-delete-{{$id_rol}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('security.rol.modal')
