@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Listado de Roles <a href="" data-target="#modal-create-rol" onclick="llenar_permisos();" data-toggle="modal"><button class="btn btn-default"><i class="fa fa-plus-circle text-green"></i> Nuevo</button></a></h3>
    </div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Opciones</th>
                </thead>

            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('security.rol.create')
@push ('scripts')
  <script>
  $(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    $('#table_id').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'ocurrio un error', location.reload() );
    } ).DataTable( {
      "order": [[ 0, 'desc' ]],
      "processing": false,
      "infoFiltered":false,
      "ordering":true,
      "paging":true,
      "stateSave": true,
      "stateDuration": -1,
        "serverSide": true,
        "ajax": "/api/roles",
        "columns":[
          {data:'id_rol',searchable: false},
          {data:'name'},
          {data:'description'},
          {data:'btn',searchable: false,"orderable": false},
        ],
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        }

    } );
} );

function abrir_edit(i)
{
    $.get('/security/rol/'+i, function (data) {
        var mod ='';
        var activo=[];
        for (var a=0; a<data.length; ++a)
        {
            if(data[a].type=='1')
            {
                mod += '<tr><td colspan="2"><input type="hidden" id="id_mod_e_'+i+'_'+data[a].id_module+'" name="mod[]" value="'+data[a].id_module+'" ><strong>'+data[a].name+'</strong></td><td class="text-center">';
                if(data[a].permit=='1')
                {
                    mod+='<input type="hidden" name="permit[]" id="permit_e_'+i+'_'+data[a].id_module+'" value="1"><input type="checkbox" checked id="check_permit_edit_'+i+'_'+data[a].id_module+'" onclick="cambio_permit_edit('+i+','+data[a].id_module+')"></td></tr>';
                    activo.push(data[a].id_module);
                }
                else{
                    mod+='<input type="hidden" name="permit[]" id="permit_e_'+i+'_'+data[a].id_module+'" value="0"><input type="checkbox" id="check_permit_edit_'+i+'_'+data[a].id_module+'" onclick="cambio_permit_edit('+i+','+data[a].id_module+')"></td></tr>';
                }
                for(var b=0; b<data.length; ++b)
                {
                    if(data[b].id_super==data[a].id_module)
                    {
                        mod += '<tr><td width="5%"></td><td><input type="hidden" id="id_mod_e_'+i+'_'+data[b].id_module+'" name="mod[]" value="'+data[b].id_module+'" >'+data[b].name+'</td><td class="text-center">';
                
                        if(data[b].permit=='1')
                        {
                            mod+='<input type="hidden" name="permit[]" class="ine_'+i+'_'+data[b].id_super+'" id="permit_e_'+i+'_'+data[b].id_module+'" value="1"><input type="checkbox" class="e_'+i+'_'+data[b].id_super+'" id="check_permit_edit_'+i+'_'+data[b].id_module+'" checked onclick="cambio_permit2_edit('+i+','+data[b].id_module+')"></td></tr>';
                        }
                        else{
                            mod+='<input type="hidden" name="permit[]" class="ine_'+i+'_'+data[b].id_super+'" id="permit_e_'+i+'_'+data[b].id_module+'" value="0"><input type="checkbox" class="e_'+i+'_'+data[b].id_super+'" id="check_permit_edit_'+i+'_'+data[b].id_module+'" disabled onclick="cambio_permit2_edit('+i+','+data[b].id_module+')"></td></tr>';
                        }
                    }
                }
                
            }
        }
        $('#lista_permisos_edit'+i).html(mod);
        for(var j=0; j<activo.length; j++)
        {
            $('.e_'+i+'_'+activo[j]).prop('disabled', false);
        }
    });
}

function llenar_permisos()
{
    var data = {!!  json_encode($modules) !!};

	var mod ='';
	for (var a=0; a<data.length; ++a)
	{
        if(data[a].type=='1')
        {
            mod += '<tr><td colspan="2"><input type="hidden" id="id_mod_c'+data[a].id_module+'" name="mod[]" value="'+data[a].id_module+'" ><strong>'+data[a].name+'</strong></td><td class="text-center">	<input type="hidden" name="permit[]" id="permit_c'+data[a].id_module+'" value="0"><input type="checkbox" id="check_permit_create'+data[a].id_module+'" onclick="cambio_permit_create('+data[a].id_module+')"></td></tr>';
        }
        else{
            mod += '<tr><td width="5%"></td><td><input type="hidden" id="id_mod_c'+data[a].id_module+'" name="mod[]" value="'+data[a].id_module+'" >'+data[a].name+'</td><td class="text-center">	<input type="hidden" name="permit[]" class="inp'+data[a].id_super+'" id="permit_c'+data[a].id_module+'" value="0"><input type="checkbox" class="'+data[a].id_super+'" id="check_permit_create'+data[a].id_module+'" disabled onclick="cambio_permit2_create('+data[a].id_module+')"></td></tr>';
        }
	}
    $('#lista_permisos_create').html(mod);
}

function cambio_permit_create(a)
{
	var checkBox = document.getElementById('check_permit_create'+a);

	if (checkBox.checked == true){
		$('#permit_c'+a).val(1);
        $('.'+a).prop('disabled', false);
        $('.'+a).prop('checked', true);	
        $('.inp'+a).val(1);
	}
    else{
		$('#permit_c'+a).val(0);
        $('.'+a).prop('disabled', true);
        $('.'+a).prop('checked', false);
        $('.inp'+a).val(0);
	}
}

function cambio_permit2_create(a)
{
	var checkBox = document.getElementById('check_permit_create'+a);

	if (checkBox.checked == true){
		$('#permit_c'+a).val(1);
        
	}
    else{
		$('#permit_c'+a).val(0);
	}
}


function cambio_permit_edit(i,a)
{
	var checkBox = document.getElementById('check_permit_edit_'+i+'_'+a);

	if (checkBox.checked == true){
		$('#permit_e_'+i+'_'+a).val(1);
        $('.e_'+i+'_'+a).prop('disabled', false);
        $('.e_'+i+'_'+a).prop('checked', true);	
        $('.ine_'+i+'_'+a).val(1);
	}
    else{
        $('#permit_e_'+i+'_'+a).val(0);
        $('.e_'+i+'_'+a).prop('disabled', true);
        $('.e_'+i+'_'+a).prop('checked', false);
        $('.ine_'+i+'_'+a).val(0);
	}
}

function cambio_permit2_edit(i,a)
{
	var checkBox = document.getElementById('check_permit_edit_'+i+'_'+a);

	if (checkBox.checked == true){
		$('#permit_e_'+i+'_'+a).val(1);
        
	}
    else{
		$('#permit_e_'+i+'_'+a).val(0);
	}
}
</script>
@endpush
@endsection
