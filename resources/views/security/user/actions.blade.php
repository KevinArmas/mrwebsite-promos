<a href="" data-target="#modal-edit-{{$id}}" onclick="abrir_edit({{$id}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('security.user.edit')
<a href="" data-target="#modal-delete-{{$id}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('security.user.modal')
