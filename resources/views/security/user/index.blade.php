@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Listado de Usuarios <a href="" data-target="#modal-create-user" data-toggle="modal"><button class="btn btn-default"><i class="fa fa-plus-circle text-green"></i> Nuevo</button></a></h3>
    </div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Correo Electrónico</th>
                    <th>Rol</th>
                    <th>Sucursal</th>
                    <th>Opciones</th>
                </thead>

            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('security.user.create')
@push ('scripts')
  <script>
  $(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    $('#table_id').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'ocurrio un error', location.reload() );
    } ).DataTable( {
      "order": [[ 0, 'desc' ]],
      "processing": false,
      "infoFiltered":false,
      "ordering":true,
      "paging":true,
      "stateSave": true,
      "stateDuration": -1,
        "serverSide": true,
        "ajax": "/api/users",
        "columns":[
          {data:'id',searchable: false},
          {data:'name'},
          {data:'email'},
          {data:'rol', name:'rol.name'},
          {data:'sucursal', name:'sucursal.name'},
          {data:'btn',searchable: false,"orderable": false},
        ],
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        }

    } );
} );

function abrir_edit(i)
{
    var brand=$('#id_rol'+i).val();
    var type=$('#id_sucursal'+i).val();
	var data = {!!  json_encode($rol) !!};
    var data2 = {!!  json_encode($sucursal) !!};

	var prov ='<option value= "">- Seleccione -</option>';
	for (var a=0; a<data.length; ++a)
	{
    if(brand==data[a].id_rol)
    {
      prov += '<option selected value= "'+data[a].id_rol+'">'+data[a].name+'</option>';
    }
    else{
      prov += '<option value= "'+data[a].id_rol+'">'+data[a].name+'</option>';
    }
	}
	$('#id_rol'+i).html(prov);

	$('#id_rol'+i).select2({
        dropdownParent: $('#modal-edit-'+i),
        dropdownPosition: 'below',
        theme: 'themeb',
  });

  var prov2 ='<option value= "">- Seleccione -</option>';
	for (var a=0; a<data2.length; ++a)
	{
    if(type==data2[a].id_sucursal)
    {
      prov2 += '<option selected value= "'+data2[a].id_sucursal+'">'+data2[a].name+'</option>';
    }
    else{
      prov2 += '<option value= "'+data2[a].id_sucursal+'">'+data2[a].name+'</option>';
    }
	}
	$('#id_sucursal'+i).html(prov2);

	$('#id_sucursal'+i).select2({
        dropdownParent: $('#modal-edit-'+i),
        dropdownPosition: 'below',
        theme: 'themeb',
  });
}
</script>
@endpush
@endsection
