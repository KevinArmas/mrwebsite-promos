<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$id}}" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Editar Usuario</h4>
			</div>
			{!!Form::open(array('url'=>'editar/user','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'onsubmit'=>'return checkSubmit();'))!!}
			<input type="hidden" name="id" value="{{$id}}">
			<div class="modal-body" style="min-height:250px; ">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Nombre</label>
						<input id="name" type="text" class="form-control" placeholder="Nombre..." required name="name" value="{{$name}}">
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Correo Electrónico</label>
						<input id="email" type="email" class="form-control" placeholder="Correo Electrónico..." required name="email" value="{{$email}}">
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Password</label>
						<input id="password" type="password" placeholder="Password..." class="form-control" name="password">
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Confirmar Password</label>
						<input id="password_conf" type="password" placeholder="Password..." class="form-control" name="password_confirmation">
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Rol</label>
						<select id="id_rol{{$id}}" style="width:100%" name="id_rol" class="form-control select2" required>
							<option value="{{$id_rol}}">{{$rol}}</option>
						</select>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label>Sucursal</label>
						<select id="id_sucursal{{$id}}" style="width:100%" name="id_sucursal" class="form-control select2" required>
							<option value="{{$id_sucursal}}">{{$sucursal}}</option>
						</select>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
			</div>
			{{Form::Close()}}
		</div>
	</div>
</div>
@push('scripts')
<script>
enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
	if (!enviando) {
	enviando= true;
	return true;
	}
	else {
	//Si llega hasta aca significa que pulsaron 2 veces el boton submit
	alert("El formulario ya se esta enviando");
	return false;
	}
}
</script>
@endpush
