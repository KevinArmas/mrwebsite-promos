<a href="" data-target="#modal-edit-{{$id_sucursal}}" onclick="abrir_edit({{$id_sucursal}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('security.sucursal.edit')
<a href="" data-target="#modal-delete-{{$id_sucursal}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('security.sucursal.modal')
