<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$id_sucursal}}">
	{{Form::Open(array('action'=>array('SucursalController@destroy',$id_sucursal),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Anular Sucursal</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Anular la sucursal</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
				<button type="submit" class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Confirmar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
