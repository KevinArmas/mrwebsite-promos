<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$id_sucursal}}" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Editar Sucursal</h4>
			</div>
		{!!Form::open(array('url'=>'editar/sucursal','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'onsubmit'=>'return checkSubmit();'))!!}
		<input type="hidden" name="id" value="{{ $id_sucursal}}">
<div class="modal-body" style="min-height:250px; ">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Nombre</label>
			<input type="text" class="form-control" required placeholder="Nombre..." name="name" value="{{$name}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Dirección</label>
			<input type="text" class="form-control" required name="adress" placeholder="Dirección..." value="{{$adress}}">
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label>Descripción</label>
			<textarea class="form-control" placeholder="Descripción..." name="description">{{$description}}</textarea>
		</div>
	</div>

</div>
<div class="modal-footer">
	<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
</div>
{{Form::Close()}}
</div>
</div>
</div>
@push('scripts')
<script>
enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
	if (!enviando) {
	enviando= true;
	return true;
	}
	else {
	//Si llega hasta aca significa que pulsaron 2 veces el boton submit
	alert("El formulario ya se esta enviando");
	return false;
	}
}
</script>
@endpush
