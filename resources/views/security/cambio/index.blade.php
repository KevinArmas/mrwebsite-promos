@extends ('layouts.admin')
@section ('contenido')
<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Cambiar Contraseña: {{$user->name}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('message'))
			<div id="alerta" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  {{Session::get('message')}}
			</div>
			@endif
			{!!Form::model($user,['method'=>'PATCH','route'=>['security.cambio.update',$user->id]])!!}
			{{Form::token()}}
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
																	<label for="password" class="col-md-4 control-label">Password</label>

																	<div class="col-md-6">
																			<input id="password" type="password" class="form-control" name="password">

																			@if ($errors->has('password'))
																					<span class="help-block">
																							<strong>{{ $errors->first('password') }}</strong>
																					</span>
																			@endif
																	</div>
															</div>

															<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
																	<label for="password-confirm" class="col-md-4 control-label">Confirmar Password</label>

																	<div class="col-md-6">
																			<input id="password-confirm" type="password" class="form-control" name="password_confirmation">

																			@if ($errors->has('password_confirmation'))
																					<span class="help-block">
																							<strong>{{ $errors->first('password_confirmation') }}</strong>
																					</span>
																			@endif
																	</div>
															</div>
															</div>
									<div class="form-group col=lg-12 col-sm-12 col-md-12 col-xs-12">
										<button class="btn btn-primary" type="submit">Guardar</button>
									</div>
									{!!Form::close()!!}


		</div>
	</div>
@endsection
