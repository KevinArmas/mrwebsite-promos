@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <h3>Información General de la Empresa <a href="{{URL::action('EmpresaController@edit',$empresa->id_empresa)}}"><button class="btn btn-info">Editar</button></a> </h3>
    </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
                <tr>
                  <td style="width:30%">Nombre de la Empresa</td>
                  <th>{{$empresa->nombre}}</th>
                </tr>
                <tr>
                  <td>Nombre Comercial</td>
                  <th>{{$empresa->nombre_comercial}}</th>
                </tr>
                <tr>
                  <td>NIT</td>
                  <th>{{$empresa->nit}}</th>
                </tr>
                <tr>
                  <td>Dirección</td>
                  <th>{{$empresa->direccion}}</th>
                </tr>
                <tr>
                  <td>Teléfono</td>
                  <th>{{$empresa->telefono}}</th>
                </tr>
                <tr>
                  <td>Correo Electrónico</td>
                  <th>{{$empresa->correo}}</th>
                </tr>
                <tr>
                  <td>Cliente</td>
                  <th>{{$empresa->num_cliente}}</th>
                </tr>
                <tr>
                  <td>Serie</td>
                  <th>{{$empresa->serie}}</th>
                </tr>
                <tr>
                  <td>Símbolo de moneda</td>
                  <th>{{$empresa->moneda}}</th>
                </tr>
                <tr>
                  <td>IVA</td>
                  <th>{{$empresa->iva}}</th>
                </tr>
                <tr>
                  <td>Logo</td>
                  <th><img src="{{asset('/img/'.$empresa->logo)}}" heigth="100px" width="100px"></th>
                </tr>
            </table>
            <hr>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a href="{{url('menu/empresa')}}"><button class="btn btn-warning">Ordenar Menus</button></a>
  </div>
</div>
@endsection
