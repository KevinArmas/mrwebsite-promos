@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Editar datos de la Empresa</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

{!!Form::model($empresa,['method'=>'PATCH','route'=>['configurations.empresa.update',$empresa->id_empresa], 'files'=>'true'])!!}
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Nombre de la Empresa</label>
            <input type="text" name="nombre" maxlength="200" required class="form-control" value="{{ $empresa->nombre }}" placeholder="Nombre de la Empresa...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Nombre Comercial</label>
            <input type="text" name="nombre_comercial" required maxlength="200" class="form-control" value="{{ $empresa->nombre_comercial }}" placeholder="Nombre Comercial...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>NIT</label>
            <input type="text" name="nit" maxlength="15" required class="form-control" value="{{ $empresa->nit }}" placeholder="NIT...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Dirección</label>
            <input type="text" name="direccion" maxlength="100" required class="form-control" value="{{ $empresa->direccion}}" placeholder="Dirección...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Teléfono</label>
            <input type="text" name="telefono" maxlength="20" required class="form-control" value="{{ $empresa->telefono}}" placeholder="Teléfono...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Correo Electrónico</label>
            <input type="text" name="correo" maxlength="100" class="form-control" value="{{ $empresa->correo}}" placeholder="Correo Electrónico...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Cliente</label>
            <input type="text" name="num_cliente" required class="form-control" value="{{ $empresa->num_cliente}}" placeholder="Cliente...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Serie</label>
            <input type="text" name="serie" required  class="form-control" value="{{ $empresa->serie}}" placeholder="Serie...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>Símbolo de Moneda</label>
            <input type="text" name="moneda"  required class="form-control" value="{{ $empresa->moneda}}" placeholder="Símbolo de Moneda...">
        </div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
            <label>IVA</label>
            <input type="number" name="iva" step="0.01" max="1" required class="form-control" value="{{ $empresa->iva}}" placeholder="IVA...">
        </div>
	</div>

	<div class="col=lg-6 col-sm-6 col-md-6 col-xs-12">
		<div class="form-group">
			<label>Imagen</label>
			<input type="file" name="image" class="form-control" placeholder="Imagen del artículo...">
			@if (($empresa->logo)!="")
				<img src="{{asset('/img/'.$empresa->logo)}}" heigth="200px" width="200px">
			@endif
		</div>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Guardar</button>
            <button class="btn btn-danger" type="reset" onclick='(window.location.href = "{{URL::to('configurations/empresa')}}");'>Cancelar</button>
        </div>
	</div>
</div>
{!!Form::close()!!}
@endsection
