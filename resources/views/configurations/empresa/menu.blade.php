@extends ('layouts.admin')
@section ('contenido')
<div class="row">
  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
  {!!Form::open(array('url'=>'editar/menu', 'method'=>'POST', 'autocomplete'=>'off',))!!}
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
      <h3>ORDEN DE LOS MENUS</h3>
      <label><i>Ordene el menu de forma ascendente, si cambia el orden de menú tenga en cuenta que debe modificar tambien los valores de los submenus del mismo</i></label>
      <table class="table table-striped table-bordered table-condensed table-hover text-left">
        <thead>
          <th>ORDEN</th>
          <th style="width:40%">MENU</th>
          <th style="width:40%">SUBMENU</th>
        </thead>
        @foreach($menu as $m)
        <tr>
          <th><input type="hidden" value="{{$m->id_module}}" name="id_module[]"><input required type="number" value="{{$m->ordered}}" name="ordered[]" class="form-control input-sm text-right"></th>
          <td style="width:40%">{{$m->name}}</td><td style="width:40%"></td>
        </tr>
          @foreach($submenu as $s)
            @if($s->id_super==$m->id_module)
            <tr>
              <td><input type="hidden" value="{{$s->id_module}}" name="id_module[]"><input required type="number" value="{{$s->ordered}}" name="ordered[]" class="form-control input-sm text-right"></td>
              <td style="width:40%"></td><td style="width:40%">{{$s->name}}</td>
            </tr>
            @endif
          @endforeach
        
        @endforeach
      </table>
      
      <div class="form-group">
        <button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Actualizar</button>
        <button class="btn btn-default" type="reset" onclick='(window.location.href = "{{URL::to('configurations/empresa')}}");'><i class="fa fa-close text-red"></i>Cancelar</button>
    </div>
  </div>
  {{Form::Close()}}
</div>
@endsection
