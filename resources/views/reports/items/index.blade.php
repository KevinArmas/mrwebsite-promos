@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Reporte Productos y Servicios</h3>
    </div>
</div>

<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>#</th>
					<th>Categoria</th>
                    <th>Nombre</th>
                    <th>Precio 1</th>
                    <th>Precio 2</th>
                    <th>Precio 3</th>
                    <th>Observacion</th>
                    <th>Estado</th>
                </thead>
				@foreach($item as $i)
				<tr>
					<td>{{$i->id_item}}</td>
              		<td>{{$i->namecategory}}</td>
              		<td>{{$i->nameitem}}</td>
              		<td>{{$i->price1}}</td>
              		<td>{{$i->price2}}</td>
              		<td>{{$i->price3}}</td>
                    <td>{{$i->observation}}</td>
                    <td>{{$i->status}}</td>
            	</tr>
                @endforeach
            </table>
        </div>
        <div class="clearfix"></div>
        <a target="_blank" href="{{url('pdfitem')}}"><button class="btn btn-info">Imprimir</button></a>
    </div>
</div>


@push ('scripts')
<script>
$(document).ready(function() 
{

});

</script>
@endpush
@endsection