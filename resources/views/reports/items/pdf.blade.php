<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>
    <title>REPORTE</title>
    <style>
    @page { margin-top: 20px; }
    body{
      font-family: Arial, Helvetica, sans-serif;
      letter-spacing: 0.1em;
    }
        table{
          border-collapse: collapse;
            font-size: 12px;
        }
        tr{
          page-break-inside: avoid;
        }
        td, th{
          border:1px solid;
          padding: 3px;
        }
    </style>
</head>
<body>
  <div style="position:absolute;"><img src="{{ public_path('img/logo.png') }}" height="70px" ></div>

  <div style="text-align: center;position:relative;">
      <p style="font-size: 10px"><font style="font-size: 16px"><strong>{{$empresa->nombre_comercial}}</strong></font><br>{{$empresa->direccion}}<br>Tel. {{$empresa->telefono}}<br>NIT. {{$empresa->nit}}@if($empresa->correo!='')<br>{{$empresa->correo}} @endif</p>
      <hr>
  </div>

  <div style="text-align: center">
        <label>Reporte Productos y Servicios</label>
        <hr>
  </div>

  <div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		  <div class="table-responsive">
		  	<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
          <thead>
					  <th>Categoria</th>
            <th>Nombre</th>
            <th>Precio 1</th>
            <th>Precio 2</th>
            <th>Precio 3</th>
            <th>Observacion</th>
            <th>Estado</th>
          </thead>
          @foreach($item as $i)
				  <tr>
            <td style="text-align: center;">{{$i->namecategory}}</td>
            <td style="text-align: center;">{{$i->nameitem}}</td>
            <td style="text-align: center;">{{$i->price1}}</td>
            <td style="text-align: center;">{{$i->price2}}</td>
            <td style="text-align: center;">{{$i->price3}}</td>
            <td style="text-align: center;">{{$i->observation}}</td>
            <td style="text-align: center;">{{$i->status}}</td>
          </tr>
          @endforeach
        </table>
      </div>
      <div class="clearfix"></div>
  </div>
</div>

</body>
</html>
