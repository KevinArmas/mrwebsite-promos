@extends ('layouts.admin')
@section ('contenido')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Nueva Venta</h3>
  </div>
</div>

@if(Session::has('message'))
  <div id="alerta" class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    {{Session::get('message')}}
	</div>
@endif

<!-- Listado de Mesas con estado -->
<div class="row">
  <?php
    $cont = 0;
    foreach ($mesas as $mesa) 
    {if($cont==0){
  ?>
    <?php } ?>
    <div class="card col-lg-4 col-md-4 col-sm-4 col-xs-12 conte">
      <img height="200px" src="/img/mesa.jpg" />

      @if($mesa->status == "Disponible")
      <div class="thumbnail" style="background-color:green"></div>
        <button class="centrado" style="background-color:green;color:white">
      
      @elseif($mesa->status == "Ocupada")
      <div class="thumbnail" style="background-color:red"></div>
        <button class="centrado" style="background-color:red;color:white">

      @elseif($mesa->status == "Limpieza")
      <div class="thumbnail" style="background-color:blue"></div>
        <button class="centrado" style="background-color:blue;color:white">

      @endif

          <h3><b>Mesa No.{{ $mesa->numero }}</b></h3>
          <h4><b>{{ $mesa->status }}</b></h4><br>
        </button>
      
    </div>
    <?php
      if($cont==2)
      {$cont = -1;?>
  <?php
  }
  $cont++;}
  if($cont<2){?>
  <?php } ?>
  </div>
</div>
<!-- fin del listado de mesas -->

<!-- Pedido -->
{!!Form::open(array('url'=>'ventas/caja','method'=>'POST','autocomplete'=>'off'))!!}
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="table-responsive">
      <table width='100%'>
        <thead>
          <th class="bg-info text-center" width="10%">Opc.</th>
					<th class="bg-info text-center" width="60%">Productos</th>
          <th class="bg-info text-center" width="10%">Cantidad</th>
					<th class="bg-info text-center" width="10%">Precio</th>
        </thead>
        <tbody id="services"></tbody>
        <input type="hidden" id='linea_s' value="0">
        <tr>
          <td><button class="btn btn-default" type="button" onclick="agg_service();"><i class="fa fa-plus text-yellow"></i></button></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td><label>Total:</label></td>
          <td><input type="num" class="form-control input-sm" name="subtotal" id="subtotal" readonly></td>
        </tr>
      </table>
    </div>
    <div class="clearfix"></div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <button class="btn btn-primary" type="submit">Guardar</button>
        <button class="btn btn-danger" type="reset" onclick='(window.location.href = "{{URL::to('ventas/caja')}}");'>Cancelar</button>
      </div>
    </div>
  </div>
{{Form::Close()}}
<!-- fin del pepido -->

@push ('scripts')
<script>

  $(document).ready(function() 
  {
    $('div.alert').not('.alert-important').delay(5000).slideUp(500);
  });

  function agg_service() {
    var e = $('#linea_s').val();

    newsv='<tr id="l_s-'+e+'"><td class="text-center"><button class="btn btn-warning btn-xs" id="btd_service-'+e+'" type="button" onclick="borrar_l_service('+e+')"><i class="fa fa-close"></i></button></td><td><input type="hidden" id="id-'+e+'" name="id[]" value="0"> <select id="list_service-'+e+'" style="width:100%" onchange="price_service('+e+')" class="form-control input-sm select2"><option value="'+e+'">- Seleccione -</option></select><input type="hidden" id="id_service-'+e+'" name="id_service[]"></td><td><input type="number" class="form-control input-sm" id="cantidad-'+e+'" name="cantidad[]" placeholder="1"><td><input type="number" class="form-control input-sm" id="price-'+e+'" name="sale_price[]" placeholder="0" readonly><input type="hidden" id="buy_price-'+e+'" name="buy_price[]"></td></tr>';

    $('#services').append(newsv);
    $('#list_service-' + e).select2({
      dropdownAutoWidth: false,
      minimumInputLength: 1
    });
    $('#list_service-' + e).on('select2:opening', llenar_s);
    e++;
    $('#linea_s').val(e);
  }

  function llenar_s()
{
    var i = $(this).val();
    var service_addons='';
	  var data_s = {!!  json_encode($item) !!};
    var op_s ='<option value= "">- Seleccione -</option>';
    
    var b = i.indexOf('_');
    if(b == -1)
    {
        for (var a=0; a<data_s.length; ++a)
	    {
	    	op_s += '<option value= "'+data_s[a].id_item+'_'+data_s[a].price+'">'+data_s[a].name+'</option>';
	    }

        op_s+=service_addons;
        $('#list_service-'+i).html(op_s);
    }
}

function price_service(i)
{
    var itemData = document.getElementById('list_service-'+i).value.split('_');
    var id_s = itemData[0];

    $('#id_service-'+i).val(itemData[0]);

    if(id_s!='')
    {
      var html = '<option value="">- Seleccione -</option><option value="1">Precio 1</option><option value="2">Precio 2</option><option value="3">Precio 3</option>';

      $('#select_price-'+i).html(html);

      $('#select_price-'+i).removeAttr('disabled');
        
      $('#price-'+i).val(itemData[1]);
      $('#buy_price-'+i).val(itemData[1]);
      $('#select_price-'+i).val(1);
      $('#save_price-'+i).val("Precio 1");    
      
      sum_price();   
    }
}

function sum_price()
{
    var valor = $('#linea_s').val();
    var sum = 0;
    for (var a = 0; a < valor; a++)
    {
        var price = $('#price-'+a).val();
        if(price == '')
        {
            price = 0;
            sum += parseFloat(price);
        }
        sum += parseFloat(price);
    }

	$('#subtotal').val(sum.toFixed(2));
}

function cambio_precio(i)
{
    var precio = $('#select_price-'+i).val();
    var itemData = document.getElementById('list_service-'+i).value.split('_');
    if (precio == '1')
    {
        $('#price-'+i).val(itemData[1]);
        $('#buy_price-'+i).val(itemData[1]);
        $('#save_price-'+i).val("Precio 1");
    }
    else if (precio == '2')
    {
        $('#price-'+i).val(itemData[2]);
        $('#buy_price-'+i).val(itemData[2]);
        $('#save_price-'+i).val("Precio 2");
    }
    else if (precio == '3')
    {
        $('#price-'+i).val(itemData[3]);
        $('#buy_price-'+i).val(itemData[3]);
        $('#save_price-'+i).val("Precio 3");
    }
    else if (precio != '1' && precio != '2' && precio != '3' & precio != '')
    {
        $('#price-'+i).val(precio);
        $('#buy_price-'+i).val(precio);
    }
	sum_price();
}

function cambio_precio_p(i)
{
    var precio = $('#select_pricep-'+i).val();
    var itemData = document.getElementById('list_part-'+i).value.split('_');
    if (precio == '1')
    {
        $('#pricep-'+i).val(itemData[1]);
        $('#buy_pricep-'+i).val(itemData[1]);
        $('#save_pricep-'+i).val("Precio 1");
    }
    else if (precio == '2')
    {
        $('#pricep-'+i).val(itemData[2]);
        $('#buy_pricep-'+i).val(itemData[2]);
        $('#save_pricep-'+i).val("Precio 2");
    }
    else if (precio == '3')
    {
        $('#pricep-'+i).val(itemData[3]);
        $('#buy_pricep-'+i).val(itemData[3]);
        $('#save_pricep-'+i).val("Precio 3");
    }
    else if (precio != '1' && precio != '2' && precio != '3' & precio != '')
    {
        $('#pricep-'+i).val(precio);
        $('#buy_pricep-'+i).val(precio);
    }
}

  function borrar_l_service(i) {
    var id = $('#id-' + i).val();
    $('#l_s-' + i).remove();
    var e = $('#linea_s').val();
    for (var a = i + 1; a < e; a++) {
      $('#l_s-' + a).attr("id", 'l_s-' + i);
      $('#id-' + a).attr("id", 'id-' + i);
      $('#btd_service-' + a).attr("id", 'btd_service-' + i);
      $('#btd_service-' + i).removeAttr("onclick");
      $('#btd_service-' + i).attr("onclick", 'borrar_l_service(' + i + ')');
      $('#list_service-' + a).attr("id", 'list_service-' + i);
      $('#id_service-' + a).attr("id", 'id_service-' + i);
      $('#price-' + a).attr("id", 'price-' + i);
      $('#buy_price-' + a).attr("id", 'buy_price-' + i);
      $('#select_price-' + a).attr("id", 'select_price-' + i);
      $('#list_service-' + i).removeAttr("onchange");
      $('#select_price--' + i).removeAttr("onchange");
      $('#list_service-' + i).attr("onchange", 'price_service(' + i + ')');
      $('#select_price-' + i).attr("onchange", 'cambio_precio(' + i + ')');

      var ex = $('#list_service-' + i).val();
      var b = ex.indexOf('_');
      if (b == -1) {
        ex--;
        var text = $('#list_service-' + i + ' option:selected').text();
        $('#list_service-' + i).html('<option selected value="' + ex + '" >' + text + '</option>');
      }

      i++;
    }
    e--;
    $('#linea_s').val(e);
    sum_price();
  }
</script>
@endpush
@endsection