<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-show" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Detalle de la Venta</h4>
			</div>

			<div class="modal-footer">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<div class="panel panel-primary panel-body">
					
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<label># Venta</label>
								<p id="nventa"></p>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<label>Usuario</label>
								<p id="cajero"></p>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<label>Estatus</label>
								<p id="estado"></p>
							</div>
						</div>

						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<label>Total Ventido</label>
								<p id="venta"></p>
							</div>
						</div>

					</div>

					<div class="panel panel-primary panel-body">
						<div class="table-responsive"  style="height: 150px; overflow-y: scroll;">
							<table class="table table-striped table-bordered table-condensed table-hover">
								<thead>
									<tr>
										<th class="bg-info text-center" width="80%">Producto</th>
										<th class="bg-info text-center">Precio</th>
									</tr>
								</thead>
								<tbody id="det-venta">
									
								</tbody>
							</table>
						</div>
					</div>

				</div>

				<div id="borrar-msj2" hidden class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				</div>

				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
			</div>
			
		</div>
	</div>
</div>
@push ('scripts')
<script>
	function abrir_detalles(i)
	{
		$.get('/ventas/ventas/'+i, function (data) {
			$('#nventa').html(data[0][0].id_venta);
			$('#cajero').html(data[0][0].user);
			$('#estado').html(data[0][0].status);
			$('#venta').html("Q." + data[0][0].total);

			var html='';
			for(var a=0; a<data[1].length; a++)
			{
				html+='<tr><td>'+data[1][a].name+'</td><td>Q.'+data[1][a].sale_price+'</td></tr>';
			}
			$('#det-venta').html(html);
		});
	}
</script>
@endpush
