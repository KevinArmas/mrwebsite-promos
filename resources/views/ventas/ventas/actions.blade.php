@if($status == "ACTIVO")
    <a href="" data-target="#modal-show" onclick="abrir_detalles({{$id_venta}})" data-toggle="modal">
        <button class="btn btn-sm btn-default"><i class="fa fa-list text-green"></i> Detalles</button>
    </a>

    <a href="" data-target="#modal-delete-{{$id_venta}}" data-toggle="modal">
        <button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button>
    </a>
@else
    <a href="" data-target="#modal-show" onclick="abrir_detalles({{$id_venta}})" data-toggle="modal">
        <button class="btn btn-sm btn-default"><i class="fa fa-list text-green"></i> Detalles</button>
    </a>
@endif
@include('ventas.ventas.modal')
@include('ventas.ventas.detail')
