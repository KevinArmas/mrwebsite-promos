<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MrWebsite Promos</title>
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('css/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.min.css')}}">
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
    <!-- custom-css -->
    <link rel="stylesheet" href="{{asset('css/custom.min.css')}}">
  </head>

  <body class="hold-transition layout-top-nav">
    <div class="wrapper">

      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="../img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>

        <!-- Navbar -->
      <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
          <a class="navbar-brand" href="/">
            <img src="../img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">MrWebsite Promos</span>
          </a>

          <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="{{url('aplicaciones')}}" class="nav-link">APLICACIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('soluciones')}}" class="nav-link">SOLUCIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('precios')}}" class="nav-link">PRECIOS</a>
              </li>
              <li class="nav-item">
                <a href="{{url('contacto')}}" class="nav-link">CONTACTO</a>
              </li>
              <li class="nav-item">
                <a href="{{url('recursos')}}" class="nav-link">RECURSOS</a>
              </li>
            </ul>

          </div>

          <!-- Right navbar links -->
          <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item dropdown user-menu">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="../img/avatar5.png" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">Invitado</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                  <img src="../img/avatar5.png" class="img-circle elevation-2" alt="User Image">
                  <p>
                  Invitado
                    <small>Registrarse o Inicia Sesión</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <a href="{{url('register')}}" class="btn btn-default btn-flat">Registrarse</a> -->
                  <!-- <a href="{{url('login')}}" class="btn btn-default btn-flat float-right">Iniciar Sesion</a> -->
                  <a type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#registroModal">Registrarse</a>
                  <a type="button" class="btn btn-default btn-flat float-right" data-toggle="modal" data-target="#loginModal">Iniciar Sesion</a>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="flag-icon flag-icon-gt"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-0">
                <a href="#" class="dropdown-item active">
                  <i class="flag-icon flag-icon-us mr-2"></i> English
                </a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /.navbar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="">
            <div class="">

              <div class="banner section section-solid-blue">
                <div class="row">
                  <div class="banner-icon">
                    <img class="img-circle" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/getengagement.png" alt="User Avatar">
                  </div>
                  <h1 class="title">SOLUCIONES ENGAGEMENT</h1>
                  <hr>
                </div>
              </div>

              <div class="section section-solid-gray">
                <div class="row">
                  <div class="bigger-paragraph">
                    <p>Las herramientas y apps de Easypromos te permiten trabajar el engagement con tu comunidad online y offline. Nuestro objetivo es ofrecer soluciones a los equipos de marketing para relacionarse con sus audiencias, incentivar el compromiso con la marca, y adquirir datos de contacto de los usuarios interesados para poder activar acciones de marketing relacional.</p>
                  </div>
                </div>
                <!-- /.row -->
              </div>

              <div class="section section-learn">
                <div class="row">
                  <div class="">
                    <h2 class="content-title">DESCUBRE LAS SOLUCIONES DE EASYPROMOS PARA APLICAR CON ÉXITO TU ESTRATEGIA DE MARKETING RELACIONAL</h2>
                  </div>
                </div>
                <!-- /.row -->
                <div class="row two-columns-row">
                  <div class="col">
                    <h4>Soluciones de gamificación</h4>
                    <p>Genera dinámicas basadas en la diversión, entretenimiento y el juego así como sus atributos (la competitividad y la destreza) para atraer a la audiencia y conseguir que interactúe con la marca.</p>
                    <div class="button-container">
                      <a title="Gamification solutions" href="#" class="button">MÁS INFO</a>
                    </div>
                  </div>
                  <div class="col img-container">
                    <img src="https://m8r8j2c8.stackpathcdn.com/real/gamificacion-engagement.webp" alt="Gamification solutions">
                  </div>
                </div>
                <!-- /.row -->
                <div class="row two-columns-row">
                  <div class="col img-container">
                    <img src="https://m8r8j2c8.stackpathcdn.com/real/solucion-captacion-datos.webp" alt="Data Collection Solutions">
                  </div>
                  <div class="col">
                    <h4>Soluciones de captación de datos</h4>
                    <p>Capta la información de contacto de los usuarios y obtén su consentimiento para recibir tus campañas promocionales. Alimenta tus plataformas de email marketing, CRM o márketing de automatización.</p>
                    <div class="button-container">
                      <a title="Data Collection Solutions" href="#" class="button">More info</a>
                    </div>
                  </div>
                </div>
                <!-- /.row -->
                <div class="row two-columns-row">
                  <div class="col">
                    <h4>Soluciones de visibilidad de marca</h4>
                    <p>Organiza acciones que permiten aumentar la visibilidad y el reconocimiento de la marca, en las redes sociales y cualquier medio digitales.</p>
                    <div class="button-container">
                      <a title="Brand Awareness Solutions" href="#" class="button">More info</a>
                    </div>
                  </div>
                  <div class="col img-container">
                    <img src="https://m8r8j2c8.stackpathcdn.com/real/visibilidad-de-marca.webp" alt="Brand Awareness Solutions">
                  </div>
                </div>
                <!-- /.row -->
                <div class="row two-columns-row">
                  <div class="col img-container">
                    <img src="https://m8r8j2c8.stackpathcdn.com/real/fidelizacion-marca-soluciones.webp" alt="Customer Loyalty Solutions">
                  </div>
                  <div class="col">
                    <h4>Soluciones de fidelización</h4>
                    <p>Conserva y aumenta el engagement de tus clientes ofreciéndoles acciones promocionales que premien su fidelidad y compromiso con la marca.</p>
                    <div class="button-container">
                      <a title="Customer Loyalty Solutions" href="#" class="button">More info</a>
                    </div>
                  </div>
                </div>
                <!-- /.row -->
                <div class="row two-columns-row">
                  <div class="col">
                    <h4>Soluciones de promoción de producto</h4>
                    <p>Publicita un producto o servicio a través de aplicaciones que fomenten la participación de los usuarios. Las interacciones ayudan a la viralidad de la campaña en las redes sociales y otros medios digitales.</p>
                    <div class="button-container">
                      <a title="Product Promotion Solutions" href="#" class="button">More info</a>
                    </div>
                  </div>
                  <div class="col img-container">
                    <img src="https://m8r8j2c8.stackpathcdn.com/real/promocionar-producto-solucion.webp" alt="Product Promotion Solutions">
                  </div>
                </div>
                <!-- /.row -->
                <div class="row two-columns-row">
                  <div class="col img-container">
                    <img src="https://m8r8j2c8.stackpathcdn.com/real/ugc.webp" alt="User Generated Content Solutions">
                  </div>
                  <div class="col">
                    <h4>Soluciones de recopilación de UGC</h4>
                    <p>Obtén contenido generado por el usuario referente a la marca, y utilízalo para aumentar la confianza de los públicos externos hacia la empresa. Incentiva que los clientes compartan su experiencia públicamente.</p>
                    <div class="button-container">
                      <a title="User Generated Content Solutions" href="#" class="button">More info</a>
                    </div>
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.section -->
              <div class="section section-solid-blue-2">
                <div class="row">
                    <p>Hoy en día, el engagement entre público y marca se puede trabajar desde muchos ángulos. No estamos hablando ya sólo de ser seguidor y hacer “me gusta” a una publicación. El compromiso de un individuo con la marca se vislumbra al analizar todos los tipos de interacción y participación que se generan: seguir, comentar, compartir, jugar, ceder los datos de contactos, concursar subiendo una foto o un vídeo, dinámicas de co-creación, etc. </p>
                    <p>Hay varias estrategias de engagement según los principales objetivos que se quieran conseguir con la campaña promocional. Para que estas estrategias sean efectivas, es primordial conseguir atraer la atención del usuario y establecer una conexión positiva entre él y la marca.</p>
                    <p>El engagement de la comunidad online es un gran indicador para valorar algunos activos intangibles de la empresa como la imagen de la marca, la información sobre los clientes o el público objetivo, la recepción de un nuevo producto en el mercado, etc. En este aspecto, una comunidad que se implica en las acciones en redes sociales que ejecuta la marca tiene un engagement positivo que suma mucho valor a la empresa. Ya no estamos hablando de una acción puntual sino de una relación a largo término y con un componente emocional: la comunidad se siente afín a los valores que transmite la marca.</p>
                </div>
                <!-- /.row -->
              </div>
              <div class="section section-solid-gray">
                <div class="row text-center">
                  <h2>CONSÚLTANOS Y OBTENDRÁS UNA RECOMENDACIÓN A MEDIDA.</h2>
                  <div class="button-container">
                    <a title="Contact Us" href="{{url('contacto')}}" class="button">CONTACTO</a>
                  </div>
                </div>
              </div>
              <!-- /.section -->
            </div>
          </div>
          <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
          <!-- Anything you want -->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2022 <a href="http://mrwebsite.com.gt">mrwebsite.com.gt</a>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    
    @include('auth.registro')
    @include('auth.sesion')

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('js/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('js/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/jquery.vmap.usa.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('js/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('js/demo.js')}}"></script> --}}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('js/dashboard.js')}}"></script>
    <!-- Slider -->
    <script src="{{asset('js/main.js')}}"></script>
</html>
