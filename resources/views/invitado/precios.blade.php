<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MrWebsite Promos</title>
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('css/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.min.css')}}">
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
    <!-- custom-css -->
    <link rel="stylesheet" href="{{asset('css/custom.min.css')}}">
    <!-- icons -->
    <link href="{{asset('open-iconic/font/css/open-iconic.css')}}" rel="stylesheet">
  </head>

  <body class="hold-transition layout-top-nav">
    <div class="wrapper">

      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="../img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>

        <!-- Navbar -->
      <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
          <a class="navbar-brand">
            <img src="../img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">MrWebsite Promos</span>
          </a>

          <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="{{url('aplicaciones')}}" class="nav-link">APLICACIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('soluciones')}}" class="nav-link">SOLUCIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('precios')}}" class="nav-link">PRECIOS</a>
              </li>
              <li class="nav-item">
                <a href="{{url('contacto')}}" class="nav-link">CONTACTO</a>
              </li>
              <li class="nav-item">
                <a href="{{url('recursos')}}" class="nav-link">RECURSOS</a>
              </li>
            </ul>

          </div>

          <!-- Right navbar links -->
          <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item dropdown user-menu">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="../img/avatar5.png" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">Invitado</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                  <img src="../img/avatar5.png" class="img-circle elevation-2" alt="User Image">
                  <p>
                  Invitado
                    <small>Registrarse o Inicia Sesión</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <a href="{{url('register')}}" class="btn btn-default btn-flat">Registrarse</a> -->
                  <!-- <a href="{{url('login')}}" class="btn btn-default btn-flat float-right">Iniciar Sesion</a> -->
                  <a type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#registroModal">Registrarse</a>
                  <a type="button" class="btn btn-default btn-flat float-right" data-toggle="modal" data-target="#loginModal">Iniciar Sesion</a>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="flag-icon flag-icon-gt"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-0">
                <a href="#" class="dropdown-item active">
                  <i class="flag-icon flag-icon-us mr-2"></i> English
                </a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /.navbar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="">
            <div class="">
              <div class="section section-main-title section-solid-gray">
                <div class="row text-center dashed">
                  <h2>Escoge tu plan</h2>
                  <p><ion-icon name="checkmark-outline"></ion-icon><a href="#" target="_blank">Registrate</a> y testea todas las apps sin hacer ningún pago.</p>
                </div>
                <!-- /.row -->
              </div>

              <div class="section section-plans section-solid-gray" style="padding-top:0;">
                <div class="row row-pricing four-columns-row">
                  <div class="col plan-column basic-plan">
                    <div class="first text-center">
                      <h3 class="title">Plan<br>Básico</h3>
                      <hr>
                      <div class="text-container">
                        <h4 class="price"><strong>$29 USD/mes</strong><br>or<br><span><strong>$290 USD</strong>/año</span></h4>
                      </div>
                      <div class="button-container">
                        <a title="Buy" href="#" class="button">Contratar</a>
                      </div>
                      <a href="#">6 apps incluidas</a>
                    </div>
                    <hr>
                    <div class="second">
                      <p><strong>Características clave:</strong></p>
                      <ul class="features">
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos en Instagram</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos en Facebook</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos en Twitter</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span><strong>Hasta 5000 comentarios</strong></li>
                      </ul>
                      <div align="center">
                        <a href="#">Más información</a>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col plan-column pro-plan">
                    <div class="first text-center">
                      <h3 class="title">Plan<br>Básico Pro</h3>
                      <hr>
                      <div class="text-container">
                        <h4 class="price"><strong>$49 USD/mes</strong><br>or<br><span><strong>$490 USD</strong>/año</span></h4>
                      </div>
                      <div class="button-container">
                        <a title="Buy" href="#" class="button">Contratar</a>
                      </div>
                      <a href="#">7 apps incluidas</a>
                    </div>
                    <hr>
                    <div class="second">
                      <p><strong>Características clave:</strong></p>
                      <ul class="features">
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos en Instagram</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos en Facebook</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos en Twitter</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span><strong>Sin limite de comentarios</strong></li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Sorteos multired</li>
                      </ul>
                      <div align="center">
                        <a href="#">Más información</a>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col plan-column premium-plan">
                    <div class="first text-center">
                      <h3 class="title">Plan<br>Premium</h3>
                      <hr>
                      <div class="text-container">
                        <h4 class="price"><strong>$199 USD/mes</strong><br>or<br><span><strong>$1999 USD</strong>/año</span></h4>
                      </div>
                      <div class="button-container">
                        <a title="Buy" href="#" class="button">Contratar</a>
                      </div>
                      <a href="#">36 apps incluidas</a>
                    </div>
                    <hr>
                    <div class="second">
                      <p><strong>Características clave:</strong></p>
                      <ul class="features">
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Concursos de fotos y videos</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Juegos personalizables y Ruletas de premios</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Gestión de cupones y bonos de descuento</li>
                      </ul>
                      <div align="center">
                        <a href="#">Más información</a>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col plan-column label-plan">
                    <div class="first text-center">
                      <h3 class="title">Plan<br>Marca Blanca</h3>
                      <hr>
                      <div class="text-container">
                        <h4 class="price"><strong>$399 USD/mes</strong><br>or<br><span><strong>$3999 USD</strong>/año</span></h4>
                      </div>
                      <div class="button-container">
                        <a title="Buy" href="#" class="button">Contratar</a>
                      </div>
                      <a href="#">41 apps incluidas</a>
                    </div>
                    <hr>
                    <div class="second">
                      <p><strong>Características clave:</strong></p>
                      <ul class="features">
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Dinámicas multietapa</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Personalización CSS y dominio propio</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Multiidioma</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Desaparece la marca 'Easypromos'</li>
                      </ul>
                      <div align="center">
                        <a href="#">Más información</a>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                  <div class="col plan-column corporate-plan">
                  <div class="first text-center">
                      <h3 class="title">Plan<br>Corporativo</h3>
                      <hr>
                      <div class="text-container">
                        <h4 class="price"><a href="#">Contáctanos para presupuesto</a></h4>
                      </div>
                      <div class="button-container">
                        <a title="Buy" href="#" class="button">Contáctanos</a>
                      </div>
                      <a href="#">41 apps incluidas</a>
                    </div>
                    <hr>
                    <div class="second">
                      <p><strong>Características clave:</strong></p>
                      <ul class="features">
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Panel propio para grandes corporaciones</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Gestión de múltiples agentes y roles</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Acceso a la API de Autologin</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Soporte exclusivo</li>
                        <li><span class="oi check-list" data-glyph="circle-check"></span>Contratos legales a medida</li>
                      </ul>
                      <div align="center">
                        <a href="#">Más información</a>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                  <hr>
                  <div class="change-currency">
                    <a href="#">Ver precios en euros</a>
                  </div>
                </div>
                <!-- /.row -->
              </div>

              <div class="section section-details section-solid-blue">
                <div class="row">
                  <h2>Apps incluidas</h2>
                  <h3>En cada plan</h3>
                  <hr>
                </div>
                <div class="row comparative-table section-solid-white">
                  <table class="table table-hover" cellspacing="10" cellpadding="0" border="0">
                    <thead>
                        <tr>
                          <th colsapan="2">&nbsp;</th>
                          <th scope="col" class="tab-title basic-plan">
                              <div>
                                <strong class="plan_label raleway">Plan<br>Básico</strong>
                              </div>
                          </th>
                          <th scope="col" class="tab-title separator pro-plan">
                              <div>
                                <strong class="plan_label raleway">Plan<br>Básico Pro</strong>
                              </div>
                          </th>
                          <th scope="col" class="tab-title premium-plan">
                              <div>
                                <strong class="plan_label raleway">Plan<br>Premium</strong>
                              </div>
                          </th>
                          <th scope="col" class="tab-title label-plan">
                              <div>
                                <strong class="plan_label raleway">Plan<br>Marca Blanca</strong>
                              </div>
                          </th>
                          <th scope="col" class="tab-title corporate-plan">
                              <div>
                                <strong class="plan_label raleway">Plan<br>Corporate</strong>
                              </div>
                          </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="row-compare group_category">
                          <td><a href="#" class="sorteo-title tab-subtitle" style="color:rgba(8, 195, 168, 0.9)"><ion-icon name="gift-sharp"></ion-icon><strong>Sorteos</strong></a></td>
                          <td colspan="2" class="separator compare">
                              <a href="#" title="Comparar Planes">Comparar Planes</a>
                          </td>
                          <td colspan="3" class="compare">
                              <a href="#" title="Comparar Planes">Comparar Planes</a>
                          </td>
                        </tr>
                        <tr>
                          <td class="row_label sorteo">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="gift-outline"></ion-icon>
                                  <a id="link_to_3" class="submit_link" href="#">Sorteo con Registro</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></td>
                        </tr>
                        <tr>
                          <td class="row_label instagram">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="logo-instagram"></ion-icon>
                                  <a id="link_to_91" class="submit_link" href="https://www.easypromosapp.com/sorteo-en-instagram/">Sorteo en Instagram</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></span></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></ion-icon></span></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></span></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></ion-icon></td>
                        </tr>
                        <tr>
                          <td class="row_label facebook">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="logo-facebook"></ion-icon>
                                  <a id="link_to_24" class="submit_link" href="">Sorteo en Facebook</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label twitter">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="logo-twitter"></ion-icon>
                                  <a id="link_to_56" class="submit_link" href="">Sorteo en Twitter</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label multired">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="git-network-outline"></ion-icon>
                                  <a id="link_to_114" class="submit_link" href="#">Sorteo multired</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label list">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="list-outline"></ion-icon>
                                  <a id="link_to_75" class="submit_link" href="#">Sorteo de un listado</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label live">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="dice-outline"></ion-icon>
                                  <a id="link_to_136" class="submit_link" href="#">Sorteo en un evento</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean separator"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr class="group_category">
                          <td colspan="6"><a href="#" class="concurso-title tab-subtitle" style="color:rgba(248, 165, 0, 0.9)"><ion-icon name="trophy-outline"></ion-icon><strong>Concursos</strong></a></td>
                        </tr>
                        <tr>
                          <td class="row_label camara">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="camera-outline"></ion-icon>
                                  <a id="link_to_10" class="submit_link" href="#">Concurso de fotos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label video">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="videocam-outline"></ion-icon>
                                  <a id="link_to_11" class="submit_link" href="#">Concurso de vídeos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label textos">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="document-text-outline"></ion-icon>
                                  <a id="link_to_12" class="submit_link" href="#">Concurso de textos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label photofun">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="happy-outline"></ion-icon>
                                  <a id="link_to_126" class="submit_link" href="#">PhotoFun</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label escenarios">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="image-outline"></ion-icon>
                                  <a id="link_to_127" class="submit_link" href="#">Escenarios</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label mencion">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="chatbubble-outline"></ion-icon>
                                  <a id="link_to_52" class="submit_link" href="#">Mención + Hashtag</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label registro">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="clipboard-outline"></ion-icon>
                                  <a id="link_to_123" class="submit_link" href="#">Hashtag + Registro</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label like">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="heart-circle-outline"></ion-icon>
                                  <a id="link_to_20" class="submit_link" href="#">Vota tu favorito</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label recluta">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="people-outline"></ion-icon>
                                  <a id="link_to_19" class="submit_link" href="#">Reclutadores</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr class="group_category">
                          <td colspan="6"><a href="#" class="juegos-title tab-subtitle" style="color:rgba(66, 81, 188, 0.9)"><ion-icon name="game-controller-outline"></ion-icon><strong>Juegos</strong></a></td>
                        </tr>
                        <tr>
                          <td class="row_label puzzle">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="extension-puzzle-outline"></ion-icon>
                                  <a id="link_to_141" class="submit_link" href="#">Puzzle</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label quiz">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="stopwatch-outline"></ion-icon>
                                  <a id="link_to_231" class="submit_link" href="#">Quiz con tiempo</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label memory">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="help-outline"></ion-icon>
                                  <a id="link_to_142" class="submit_link" href="#">Juegos de memoria</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label relation">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="contract-outline"></ion-icon>
                                  <a id="link_to_147" class="submit_link" href="#">Relaciona parejas</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label soup">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="apps-outline"></ion-icon>
                                  <a id="link_to_140" class="submit_link" href="#">Sopa de letras</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label match">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="diamond-outline"></ion-icon>
                                  <a id="link_to_247" class="submit_link" href="#">Slide &amp; Match</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label ocultos">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="search-outline"></ion-icon>
                                  <a id="link_to_143" class="submit_link" href="#">Objetos ocultos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label buscaminas">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="golf-outline"></ion-icon>
                                  <a id="link_to_224" class="submit_link" href="#">Buscaminas</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr class="group_category">
                          <td colspan="6"><a href="#" class="cuestionarios-title tab-subtitle" style="color:rgba(170, 29, 107, 0.9)"><ion-icon name="reader-outline"></ion-icon><strong>Cuestionarios</strong></a></td>
                        </tr>
                        <tr>
                          <td class="row_label trivia">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="ribbon-outline"></ion-icon>
                                  <a id="link_to_16" class="submit_link" href="#">Trivia de conocimientos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label recomendador">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="star-half-outline"></ion-icon>
                                  <a id="link_to_145" class="submit_link" href="#">Recomendador productos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label personalidad">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="person-outline"></ion-icon>
                                  <a id="link_to_45" class="submit_link" href="#">Test de personalidad</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label encuesta">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="help-circle-outline"></ion-icon>
                                  <a id="link_to_17" class="submit_link" href="#">Encuesta</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label quiniela">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="balloon-outline"></ion-icon>
                                  <a id="link_to_18" class="submit_link" href="#">Quiniela</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr class="group_category">
                          <td colspan="6"><a href="#" class="premios-title tab-subtitle" style="color:rgba(78, 175, 43, 0.9)"><ion-icon name="aperture-outline"></ion-icon><strong>Premios directos</strong></a></td>
                        </tr>
                        <tr>
                          <td class="row_label ruleta">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="aperture-outline"></ion-icon>
                                  <a id="link_to_139" class="submit_link" href="#">Ruleta de premios</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label gana">
                              <div>
                                <div class="c cutTextOnOverflow">
                                  <ion-icon name="logo-bitcoin"></ion-icon>
                                  <a id="link_to_248" class="submit_link" href="#">Rasca y gana</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-chest "><span class="bg custom_bg" style="background-color:rgb(64, 145, 35) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_251" class="submit_link" href="#">Descubre y gana</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-winners "><span class="bg custom_bg" style="background-color:rgb(57, 130, 31) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_15" class="submit_link" href="#">Momento ganador</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-promotional-code "><span class="bg custom_bg" style="background-color:rgb(51, 115, 28) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_13" class="submit_link" href="#">Distribuir cupones</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-redeem-code "><span class="bg custom_bg" style="background-color:rgb(44, 100, 24) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_21" class="submit_link" href="#">Validar códigos</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-buy-ticket "><span class="bg custom_bg" style="background-color:rgb(37, 85, 20) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_241" class="submit_link" href="#">Validar tíquets de compra</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr class="group_category">
                          <td colspan="6"><a href="#" class="multi-title tab-subtitle" style="color:rgba(245, 110, 110, 0.9)"><ion-icon name="gift-outline"></ion-icon><strong>Multietapas</strong></a></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-multi-games "><span class="bg custom_bg" style="background-color:rgb(245, 110, 110) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_228" class="submit_link" href="#">Multijuego</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-brackets "><span class="bg custom_bg" style="background-color:rgb(247, 99, 99) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_235" class="submit_link" href="#">Torneo por eliminatorias</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-tiled-gallery "><span class="bg custom_bg" style="background-color:rgb(249, 88, 88) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_244" class="submit_link" href="#">Concurso multimedia</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-multi-match-predictions "><span class="bg custom_bg" style="background-color:rgb(251, 77, 77) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_229" class="submit_link" href="#">Quiniela multijornada</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                        <tr>
                          <td class="row_label">
                              <div>
                                <span class="promo_type_icon gui_circled_icon knk-calendar "><span class="bg custom_bg" style="background-color:rgb(253, 66, 66) !important;"></span></span>
                                <div class="c cutTextOnOverflow">
                                    <a id="link_to_246" class="submit_link" href="#">Calendario de premios</a>
                                </div>
                              </div>
                          </td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean separator"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="close-outline"></ion-icon></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                          <td class="is_boolean"><ion-icon name="checkmark-outline"></td>
                        </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.section -->
            </div>
          </div>
          <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
          <!-- Anything you want -->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2022 <a href="http://mrwebsite.com.gt">mrwebsite.com.gt</a>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    
    @include('auth.registro')
    @include('auth.sesion')

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('js/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('js/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/jquery.vmap.usa.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('js/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('js/demo.js')}}"></script> --}}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('js/dashboard.js')}}"></script>
    <!-- Slider -->
    <script src="{{asset('js/main.js')}}"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
  </body>
</html>
