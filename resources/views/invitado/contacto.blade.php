<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MrWebsite Contacto</title>
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('css/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.min.css')}}">
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
  </head>

  <body class="hold-transition layout-top-nav">
    <div class="wrapper">

      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="../img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>

        <!-- Navbar -->
      <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
          <a class="navbar-brand" href="/">
            <img src="../img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">MrWebsite Promos</span>
          </a>

          <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="{{url('aplicaciones')}}" class="nav-link">APLICACIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('soluciones')}}" class="nav-link">SOLUCIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('precios')}}" class="nav-link">PRECIOS</a>
              </li>
              <li class="nav-item">
                <a href="{{url('contacto')}}" class="nav-link">CONTACTO</a>
              </li>
              <li class="nav-item">
                <a href="{{url('recursos')}}" class="nav-link">RECURSOS</a>
              </li>
            </ul>

          </div>

          <!-- Right navbar links -->
          <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item dropdown user-menu">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="../img/avatar5.png" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">Invitado</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                  <img src="../img/avatar5.png" class="img-circle elevation-2" alt="User Image">
                  <p>
                  Invitado
                    <small>Registrarse o Inicia Sesión</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <a href="{{url('register')}}" class="btn btn-default btn-flat">Registrarse</a> -->
                  <!-- <a href="{{url('login')}}" class="btn btn-default btn-flat float-right">Iniciar Sesion</a> -->
                  <a type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#registroModal">Registrarse</a>
                  <a type="button" class="btn btn-default btn-flat float-right" data-toggle="modal" data-target="#loginModal">Iniciar Sesion</a>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="flag-icon flag-icon-gt"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-0">
                <a href="#" class="dropdown-item active">
                  <i class="flag-icon flag-icon-us mr-2"></i> English
                </a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /.navbar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- /.content-header -->

        <!-- Main content -->
        
        <section>
            <article>
                <header>
                    <h2>contacto <span>siempre cerca de ti</span></h2>
                </header>
                <div class="content_with_sidebar">
                    <form id="contactForm" action='/contacto/' class="submit-form" method='post' accept-charset='UTF-8'>
                        <input type="hidden" name="loadtime" value="1654108718">
                        <fieldset>
                            <div id="spam">
                                <label for="leaveblank">Leave this blank</label>
                                <input type="text" id="leaveblank" name="leaveblank" />
                                <label for="dontchange">Do not change this</label>
                                <input type="text" value="http://" name="dontchange" />
                            </div>
        
                            <div class="input text in_top">
                                <label for="name">Nombre <span>(*)</span></label>
                                <input type="text" placeholder="" id="name" name='nom' >
                            </div>
        
                            <div class="input text">
                                <label for="last-name">Apellidos <span>(*)</span></label>
                                <input type="text" placeholder="" id="last-name" name='cognoms'>
                            </div>
        
                            <div class="input text">
                                <label for="mail">E-mail <span>(*)</span></label>
                                <input type="email" placeholder="" id="mail" class="mail" name='email'>
                            </div>
        
                            <div class="input text">
                                <label for="company">Empresa <span>(*)</span></label>
                                <input type="text" placeholder="" id="company" name='empresa'>
                            </div>
        
                            <div class="input select">
                                <label for="activity">Actividad de la empresa <span>(*)</span></label>
                                <div class="this_select">
                                    <select id="activity" name='activity'>
                                        <option value=''>-- Selecciona --</option>
                                        <option value='Publicidad y/o marketing'>Publicidad y/o marketing</option>
                                        <option value='Transporte y destinos turísticos'>Transporte y destinos turísticos</option>
                                        <option value='Hoteles y restaurantes'>Hoteles y restaurantes</option>
                                        <option value='Alimentación y bebidas'>Alimentación y bebidas</option>
                                        <option value='Salud y belleza'>Salud y belleza</option>
                                        <option value='Moda y complementos'>Moda y complementos</option>
                                        <option value='Deportes y ocio'>Deportes y ocio</option>
                                        <option value='Cultura y espectáculos'>Cultura y espectáculos</option>
                                        <option value='Centros comerciales y asociaciones'>Centros comerciales y asociaciones</option>
                                        <option value='Comunicación y medios'>Comunicación y medios</option>
                                        <option value='Educación y formación'>Educación y formación</option>
                                        <option value='Bebés y niños'>Bebés y niños</option>
                                        <option value='Animales y mascotas'>Animales y mascotas</option>
                                        <option value='Seguros y servicios financieros'>Seguros y servicios financieros</option>
                                        <option value='Hogar y decoración'>Hogar y decoración</option>
                                        <option value='Tecnología y apps'>Tecnología y apps</option>
                                        <option value='Otros'>Otros</option>
                                    </select>
                                </div>
                            </div>
        
                            <div class="input text">
                                <label for="country">País <span>(*)</span></label>
                                <input type="text" placeholder="" id="country" name="country">
                            </div>
        
                            <div class="input text">
                                <label for="subject">Asunto</label>
                                <input type="text" placeholder="" id="subject" name='assumpte'>
                            </div>
        
                            <div class="input textarea">
                                <label for="message">Mensaje <span>(*)</span></label>
                                <textarea cols="10" rows="20" id="message" name="missatge"></textarea>
                            </div>
                            <div class="input checkbox">
                                <input type="checkbox" class="error" id="toc" name="toc" aria-required="true">
                                <label for="toc">
                                    He leído y acepto la <a href="https://www.easypromosapp.com/politica-privacidad-proteccion-datos/" target="_blank">política de privacidad</a> de EASYPROMOS SL							<span data-href-blank="https://www.easypromosapp.com/info-basica-proteccion-datos/" class="knk-information is_tooltip" title="Haz clic en el icono para leer la tabla con la información básica sobre la protección de datos."></span>
                                </label>
                            </div>
        
                            <div class="input g-recaptcha" data-sitekey="6Le_AfkUAAAAAGhWM8BisJs3yeXJ904d4J1U0rFg" data-callback="enableBtn" data-expired-callback="disableBtn"></div>
                            <input type="submit" class="input submit blue_button gui_trully_disabled_button alone_button" value='enviar' id="btnSend" disabled>
                        </fieldset>
                    </form>
        
                            <aside class="technical-block plot_diamond blue">
                                <header class="gui_heading small_heading">
                                    <h2>Contacta con nosotros</h2>
                                </header>
                            
                                <div class="text">
                                    <p><strong>Chat en vivo</strong>&nbsp;&nbsp;❤️<a href="https://www.easypromosapp.com/chat" class="to_chat">Ir al chat</a>❤️</p>
                            
                                        <dl>
                                            <div>
                                                <dt>Agentes en español:</dt>
                                                <dd>
                                                    Ln-Jv de 9am - 11pm (CEST)<br />
                                                    Vn de 9am - 7pm (CEST)				
                                                </dd>
                                            </div>
                                            <div>
                                                <dt>Agentes de habla inglesa:</dt>
                                                <dd>
                                                    Ln-Vn de 9am - 00am (CEST)				
                                                </dd>
                                            </div>
                                        </dl>
                                        
                                        <div>
                                            <strong>Email</strong>
                                            <dl>
                                                <div>
                                                    <dt>Atención comercial:</dt>
                                                    <dd>h&#111;l&#97;&#64;eas&#121;&#x70;&#114;om&#111;&#115;&#x61;&#x70;p.&#99;om</dd>
                                                </div>
                                                <div>
                                                    <dt>Atención técnica:</dt>
                                                    <dd>&#x73;o&#x70;&#111;r&#116;e&#64;eas&#x79;pro&#x6d;o&#115;&#97;pp.&#99;&#111;m</dd>
                                                </div>
                                            </dl>
                                        </div>
                                </div>
                            
                                <a href="https://soporte.easypromosapp.com/hc/es/" target="_blank" title="Centro de Soporte online" class="submit black_button flex_width gui_not_in_top">Centro de Soporte online</a>
                            </aside>		
                    </div>
                </article>
            </section>
                
                
                
              </div>
              <!-- /.row -->

              

            <!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
          <!-- Anything you want -->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2022 <a href="http://mrwebsite.com.gt">mrwebsite.com.gt</a>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    
    @include('auth.registro')
    @include('auth.sesion')

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('js/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('js/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/jquery.vmap.usa.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('js/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('js/demo.js')}}"></script> --}}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('js/dashboard.js')}}"></script>
    <!-- Slider -->
    <script src="{{asset('js/main.js')}}"></script>
</html>