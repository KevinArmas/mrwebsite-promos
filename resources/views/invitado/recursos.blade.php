<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MrWebsite Recursos</title>
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('css/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.min.css')}}">
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
  </head>

  <body class="hold-transition layout-top-nav">
    <div class="wrapper">

      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="../img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>

        <!-- Navbar -->
      <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
          <a class="navbar-brand">
            <img src="../img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">MrWebsite Promos</span>
          </a>

          <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="{{url('aplicaciones')}}" class="nav-link">APLICACIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('soluciones')}}" class="nav-link">SOLUCIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('precios')}}" class="nav-link">PRECIOS</a>
              </li>
              <li class="nav-item">
                <a href="{{url('contacto')}}" class="nav-link">CONTACTO</a>
              </li>
              <li class="nav-item">
                <a href="{{url('recursos')}}" class="nav-link">RECURSOS</a>
              </li>
            </ul>

          </div>

          <!-- Right navbar links -->
          <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item dropdown user-menu">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="../img/avatar5.png" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">Invitado</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                  <img src="../img/avatar5.png" class="img-circle elevation-2" alt="User Image">
                  <p>
                  Invitado
                    <small>Registrarse o Inicia Sesión</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <a href="{{url('register')}}" class="btn btn-default btn-flat">Registrarse</a> -->
                  <!-- <a href="{{url('login')}}" class="btn btn-default btn-flat float-right">Iniciar Sesion</a> -->
                  <a type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#registroModal">Registrarse</a>
                  <a type="button" class="btn btn-default btn-flat float-right" data-toggle="modal" data-target="#loginModal">Iniciar Sesion</a>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="flag-icon flag-icon-gt"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-0">
                <a href="#" class="dropdown-item active">
                  <i class="flag-icon flag-icon-us mr-2"></i> English
                </a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /.navbar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- /.content-header -->

        <!-- Main content -->
        
        <div class="bodycard">
            <header>
                <h2>Recursos </h2>
            </header>
            <div class="container">
              <div class="row">
                <div class="col-md-4">
                  <div class="card">
                    <header class="head">
                        <h2>Aprende</h2>
                    </header>
                    <div class="description">
                        <p>Accede a los canales más habituales de generación y difusión de contenido y obtén nuevos recursos habitualmente.</p>
                    </div>
                    <ul>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" target="_blank" title="Blog">Blog</a></h3>
                                <p>Todos los artículos que genera el equipo de Easypromos sobre concursos, sorteos y promociones online.</p>
                                <div>
                                    <a href="#" target="_blank" class="submit" title="Blog">Más información</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" title="Ebooks y descargables">Ebooks y descargables</a></h3>
                                <p>Recursos gratuitos elaborados por el equipo de Easypromos que te ayudarán en la configuración de tus campañas.</p>
                                <div>
                                    <a href="#" class="submit" title="Ebooks y descargables">Más i#nformación</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" title="Suscríbete a la Newsletter">Suscríbete a la Newsletter</a></h3>
                                <p>Recibe la newsletter con los artículos y contenidos generados sobre campañas promocionales online.</p>
                                <div>
                                    <a href="#" class="submit" title="Suscríbete a la Newsletter">Más información</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="card">
                    <header class="head">
                        <h2>Descubre</h2>
                    </header>
                    <div class="description">
                        <p>Obtén ejemplos e ideas según temáticas específicas y descubre promociones reales que han conseguido sus objetivos.</p>
                    </div>
                    <ul>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" target="_blank" title="Casos de Exito">Casos de Exito</a></h3>
                                <p>Descubre casos reales de clientes que han utilizado la plataforma para lanzar sus sorteos y concursos online.</p>
                                <div>
                                    <a href="#" target="_blank" class="submit" title="Blog">Más información</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" title="Ideas por Campaña">Ideas por Campaña</a></h3>
                                <p>Ideas para crear concursos y sorteos durante las campañas más destacadas del año: días festivos, celebraciones y eventos.</p>
                                <div>
                                    <a href="#" class="submit" title="Ideas por Campaña">Más información</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" title="EasyJuegos">EasyJuegos</a></h3>
                                <p>Plataforma de mini-juegos y pasatiempos para entretener y divertir a las comunidades online.</p>
                                <div>
                                    <a href="#" class="submit" title="EasyJuegos">Más información</a>
                                </div>
                            </div>
                        </li>
                    </ul>                    
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="card">
                    <header class="head">
                        <h2>Prueba</h2>
                    </header>
                    <div class="description">
                        <p>La mejor forma de saber cómo funciona una app es probarla. Accede a las demos para conocer la experiencia del usuario.</p>
                    </div>
                    <ul>
                        <li>
                            <div class="c">
                                <h3 class="t"><a href="#" target="_blank" title="Demos y plantillas">Demos y plantillas</a></h3>
                                <p>Descarga fácilmente las plantillas pre-configuradas que hemos preparado para ahorrarte tiempo, y crea tu campaña promocional.</p>
                                <div>
                                    <a href="#" target="_blank" class="submit" title="Demos y plantillas">Más información</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                  </div>
                </div>
              </div>    
            </div> 
                
                
                
              </div>

            <!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
          <!-- Anything you want -->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2022 <a href="http://mrwebsite.com.gt">mrwebsite.com.gt</a>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    
    @include('auth.registro')
    @include('auth.sesion')

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('js/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('js/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/jquery.vmap.usa.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('js/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('js/demo.js')}}"></script> --}}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('js/dashboard.js')}}"></script>
    <!-- Slider -->
    <script src="{{asset('js/main.js')}}"></script>
</html>