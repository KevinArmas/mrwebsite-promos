
@extends ('layouts.admin')
@section ('contenido')
      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
              <img src="{{asset('/img/BISOFT.png')}}" height="100px">
            <h2>Business Solutions Software</h2>

            <a target="_blank" href="https://www.facebook.com/busissoft/" class="btn btn-social-icon btn-facebook">
              <span class="fa fa-facebook"></span>
            </a>&nbsp;
            <a target="_blank" href="https://www.instagram.com/busissoft/" class="btn btn-social-icon btn-instagram">
              <span class="fa fa-instagram"></span>
            </a>
          </div>
        </div>

@endsection
