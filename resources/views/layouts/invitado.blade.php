<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MrWebsite Promos</title>
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('css/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('css/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('css/summernote-bs4.min.css')}}">
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">
  </head>

  <body class="hold-transition layout-top-nav">
    <div class="wrapper">

      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="../img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>

        <!-- Navbar -->
      <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
        <div class="container">
          <a class="navbar-brand" href="/">
            <img src="../img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">MrWebsite Promos</span>
          </a>

          <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
              <li class="nav-item">
                <a href="{{url('aplicaciones')}}" class="nav-link">APLICACIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('soluciones')}}" class="nav-link">SOLUCIONES</a>
              </li>
              <li class="nav-item">
                <a href="{{url('precios')}}" class="nav-link">PRECIOS</a>
              </li>
              <li class="nav-item">
                <a href="{{url('contacto')}}" class="nav-link">CONTACTO</a>
              </li>
              <li class="nav-item">
                <a href="{{url('recursos')}}" class="nav-link">RECURSOS</a>
              </li>
            </ul>

          </div>

          <!-- Right navbar links -->
          <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item dropdown user-menu">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="../img/avatar5.png" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline">Invitado</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                  <img src="../img/avatar5.png" class="img-circle elevation-2" alt="User Image">
                  <p>
                  Invitado
                    <small>Registrarse o Inicia Sesión</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <!-- <a href="{{url('register')}}" class="btn btn-default btn-flat">Registrarse</a> -->
                  <!-- <a href="{{url('login')}}" class="btn btn-default btn-flat float-right">Iniciar Sesion</a> -->
                  <a type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#registroModal">Registrarse</a>
                  <a type="button" class="btn btn-default btn-flat float-right" data-toggle="modal" data-target="#loginModal">Iniciar Sesion</a>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="flag-icon flag-icon-gt"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-0">
                <a href="#" class="dropdown-item active">
                  <i class="flag-icon flag-icon-us mr-2"></i> English
                </a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /.navbar -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="">
            <div class="">

              <div class="slideshow">
                <ul class="slider">
                  <li>
                    <img src="../img/slider/2.jpg" alt="">
                    <section class="caption">
                      <h1>CREA SORTEOS Y CONCURSOS</h1>
                      <br>
                      <p>la plataforma más completa y fiable para crear promociones en Instagram, Facebook, Twitter, YouTube o en cualquier web</p>
                      <br>
                      <button class="btn btn-default btn-flat" style="border-color:#FFFFFF;">
                        <a href="{{url('soluciones')}}">VER APLICACIONES</a></button>
                      <br>
                      <br>
                      <h2>Más de <strong>20.000 clientes</strong> han creado ya más de <strong>2 millones de promociones</strong> con nosotros</h2>
                    </section>
                  </li>
                  <li>
                    <img src="../img/slider/1.jpg" alt="">
                    <section class="caption">
                      <h1>¡NUEVO EBOOK!</h1>
                      <p>¿Buscas ejemplos de promociones interactivas?</p>
                    </section>
                  </li>
                  <li>
                    <img src="../img/slider/3.jpg" alt="">
                    <section class="caption">
                      <h1>PRUEBA EL RASCA Y GANA DIGITAL</h1>
                      <button class="btn btn-warning btn-flat" style="border-color:#FFFFFF;">MÁS INFORMACIÓN</button>
                    </section>
                  </li>
                  <li>
                    <img src="../img/slider/4.jpg" alt="">
                    <section class="caption">
                      <h1>¡NUEVO EBOOK!</h1>
                      <p>Ruleta de premios online: ejemplos y objetivos para crear la tuya</p>
                    </section>
                  </li>
                </ul>

                {{-- <ol class="pagination"></ol> --}}
              
                <div class="left">
                  <span class="fa fa-chevron-left"></span>
                </div>

                <div class="right">
                  <span class="fa fa-chevron-right"></span>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-12">
                  <div class="card card-success">
                    <div class="card-body text-center">
                        <h3>MRWEBSITE PROMOS TE OFRECE</h3>
                        <h3>6 TIPOS DE APLICACIONES</h3>
                        <br>
                        <div class="row">
                          <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="card-footer">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="description-block">
                                      <h4 class="description-header">SORTEOS</h4>
                                      <p class="">Sorteos en Instagram, Facebook o Twitter entre los suscriptores de una newsletter, los registrados a un e-commerce o los asistentes a un evento, etc. Selecciona los ganadores de forma aleatoria y transparente.</p>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.row -->
                              </div>
                            </div>
                            <!-- /.widget-user -->
                          </div>
          
                          <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="card-footer">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="description-block">
                                      <h4 class="description-header">CONCURSOS</h4>
                                      <p class="">Concursos en Instagram, Facebook, Twitter o cualquier otra red social o soporte digital. Puedes pedir a los usuarios que participen con fotos, textos o vídeos, y si quieres, activar los votos para aumentar la viralidad.</p>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.row -->
                              </div>
                            </div>
                            <!-- /.widget-user -->
                          </div>
          
                          <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="card-footer">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="description-block">
                                      <h4 class="description-header">JUEGOS</h4>
                                      <p class="">Genera engagement y recuerdo de marca proporcionando diversión y entretenimiento a tu audiencia mientras captas leads. Difunde los juegos en redes sociales, tu web u otros canales.</p>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.row -->
                              </div>
                            </div>
                            <!-- /.widget-user -->
                          </div>
          
                          <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="card-footer">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="description-block">
                                      <h4 class="description-header">CUESTIONARIOS</h4>
                                      <p class="">Crea una app con preguntas y respuestas que los participantes deberán responder para acceder al premio o sorteo. Recopila datos de los participantes y obtén leads cualificados.</p>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.row -->
                              </div>
                            </div>
                            <!-- /.widget-user -->
                          </div>
          
                          <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="card-footer">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="description-block">
                                      <h4 class="description-header">PREMIOS DIRECTOS</h4>
                                      <p class="">Genera dinámicas interactivas online para repartir premios y regalos entre los participantes a cambio de conseguir sus datos de contacto. Gran abanico de opciones a configurar: ruleta de premios, rasca y gana, códigos únicos, vouchers, descargables, etc.</p>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.row -->
                              </div>
                            </div>
                            <!-- /.widget-user -->
                          </div>
          
                          <div class="col-md-4">
                            <!-- Widget: user widget style 1 -->
                            <div class="card card-widget widget-user">
                              <!-- Add the bg color to the header using any of the bg-* classes -->
                              <div class="card-footer">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="description-block">
                                      <h4 class="description-header">MULTIETAPAS</h4>
                                      <span class="">Potencia la relación con el público de tu marca con aplicaciones de larga duración y dinámicas con varias etapas y rondas. Fideliza a tu comunidad con acciones gamificadas, sistema de puntos y ranking global.</span>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.row -->
                              </div>
                            </div>
                            <!-- /.widget-user -->
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                
                
                
              </div>
              <!-- /.row -->

              <div class="row">

                <div class="col-lg-12">
                  <div class="card card-success">
                    <div class="card-body text-center">
                        <h5>MARKETING DIGITAL</h5>
                        <h3>CONSIGUE TUS OBJETIVOS</h3>
                        <h3>CON CAMPAÑAS INTERACTIVAS</h3>
                        <br>
                        <div class="row">

                          <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/getengagement.png" alt="User Avatar">
                                <h5>ENGAGEMENT</h5>
                              </div>
                            </div>
                          </div>
          
                          <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/gamificacionmkt.png" alt="User Avatar">
                                <h5>GAMIFICACIÓN</h5>
                              </div>
                            </div>
                          </div>
          
                          <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/datacollection.png" alt="User Avatar">
                                <h5>CAPTACIÓN DE DATOS</h5>
                              </div>
                            </div>
                          </div>
          
                          <div class="col-md-3">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/fidelizacionclientes.png" alt="User Avatar">
                                <h5>FIDELIZACIÓN</h5>
                              </div>
                            </div>
                          </div>
          
                          <div class="col-md-4">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/promocionproductos.png" alt="User Avatar">
                                <h5>PROMOCIÓN PRODUCTOS</h5>
                              </div>
                            </div>
                          </div>
          
                          <div class="col-md-4">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/visibilidadmarca.png" alt="User Avatar">
                                <h5>VISIBILIDAD DE MARCA</h5>
                              </div>
                            </div>
                          </div>
          
                          <div class="col-md-4">
                            <div class="card">
                              <div class="card-body">
                                <img class="img-circle" style="background-color:#4a6da7" src="https://m8r8j2c8.stackpathcdn.com/pwhimg-80-80/ugccontent.png" alt="User Avatar">
                                <h5>RECOPILACION UGC</h5>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                </div>
                
                
                
              </div>
              <!-- /.row -->

            <!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
          <!-- Anything you want -->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2022 <a href="http://mrwebsite.com.gt">mrwebsite.com.gt</a>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    
    @include('auth.registro')
    @include('auth.sesion')

    <!-- jQuery -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('js/sparkline.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('js/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('js/jquery.vmap.usa.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('js/jquery.knob.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/daterangepicker.js')}}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{asset('js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{asset('js/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    {{-- <script src="{{asset('js/demo.js')}}"></script> --}}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('js/dashboard.js')}}"></script>
    <!-- Slider -->
    <script src="{{asset('js/main.js')}}"></script>
</html>
