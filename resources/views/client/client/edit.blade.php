<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$id_client}}" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Editar Cliente</h4>
			</div>
		{!!Form::open(array('url'=>'editar/client','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'onsubmit'=>'return checkSubmit();'))!!}
		<input type="hidden" name="id" value="{{ $id_client}}">
<div class="modal-body" style="min-height:350px; ">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Nombre</label>
			<input type="text" class="form-control" required name="name" placeholder="Nombre..." value="{{$name}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Dirección</label>
			<input type="text" class="form-control" required name="adress" placeholder="Dirección..." value="{{$adress}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Telefono</label>
			<input type="number" class="form-control" required name="phone" placeholder="Teléfono..." value="{{$phone}}">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Telefono 2</label>
			<input type="number" class="form-control" name="phone2" placeholder="Teléfono..." value="{{$phone2}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Correo Electrónico</label>
			<input type="email" class="form-control" name="email" placeholder="Correo Electrónico..." value="{{$email}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>NIT</label>
			<input type="text" class="form-control" name="nit" placeholder="NIT..." value="{{$nit}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>DPI</label>
			<input type="text" class="form-control" name="dpi" placeholder="DPI..." value="{{$dpi}}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Tipo de Cliente</label>
			<select id="id_client_type_edit{{$id_client}}" name="id_client_type" class="form-control select2" style="width:100%" required>
				<option value="{{$id_client_type}}">{{$name_type}}</option>
			</select>
		</div>
	</div>


</div>
	<div class="modal-footer">
		<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
		<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
	</div>
{{Form::Close()}}
</div>
</div>
</div>
@push('scripts')
<script>
	enviando = false; //Obligaremos a entrar el if en el primer submit

	function checkSubmit() {
		if (!enviando) {
		enviando= true;
		return true;
		}
		else {
		//Si llega hasta aca significa que pulsaron 2 veces el boton submit
		alert("El formulario ya se esta enviando");
		return false;
		}
	}
</script>
@endpush
