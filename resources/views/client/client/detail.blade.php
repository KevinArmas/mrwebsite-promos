<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-show" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Detalles del Cliente</h4>
			</div>
			<div class="modal-footer">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
					<div class="panel panel-primary panel-body">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Nombre</label>
								<p id="name_show"></p>
							</div>
						</div>
					
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Dirección</label>
								<p id="adress_show"></p>
							</div>
						</div>
					
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Teléfono</label>
								<p id="phone_show"></p>
							</div>
						</div>
					
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Telefono 2</label>
								<p id="phone2_show"></p>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Correo Electrónico</label>
								<p id="email_show"></p>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>NIT</label>
								<p id="nit_show"></p>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>DPI</label>
								<p id="dpi_show"></p>
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<label>Tipo de Cliente</label>
								<p id="type_show"></p>
							</div>
						</div>
					</div>
				</div>

				<div id="borrar-msj2" hidden class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				</div>

				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
			</div>
		</div>
	</div>
</div>


@push ('scripts')
<script>
	function abrir_detalles(i)
	{
		$.get('/client/client/'+i, function (data) {
			$('#name_show').html(data[0].name);
			$('#adress_show').html(data[0].adress);
			$('#phone_show').html(data[0].phone);
			if(data[0].phone2=='')
			{
				data[0].phone2='-';
			}
			$('#phone2_show').html(data[0].phone2);
			if(data[0].email=='')
			{
				data[0].email='-';
			}
			$('#email_show').html(data[0].email);
			if(data[0].nit=='')
			{
				data[0].nit='-';
			}
			$('#nit_show').html(data[0].nit);
			if(data[0].dpi=='')
			{
				data[0].dpi='-';
			}
			$('#dpi_show').html(data[0].dpi);
			if(data[0].name_type=='')
			{
				data[0].name_type='-';
			}
			$('#type_show').html(data[0].name_type);
		});
	}


</script>
@endpush