<a href="" data-target="#modal-show" onclick="abrir_detalles({{$id_client}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-list text-green"></i> Detalles</button></a>
<a href="" data-target="#modal-edit-{{$id_client}}" onclick="abrir_edit({{$id_client}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('client.client.edit')
<a href="" data-target="#modal-delete-{{$id_client}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('client.client.modal')

