<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-create-client" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Agregar Cliente</h4>
			</div>

	{!!Form::open(array('url'=>'client/client','id'=>'env_form', 'method'=>'POST', 'autocomplete'=>'off', 'onsubmit'=>'return checkSubmit();'))!!}
<div class="modal-body" style="min-height:350px; ">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Nombre</label>
			<input type="text" class="form-control" required name="name" placeholder="Nombre..." value="{{ old('name') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Dirección</label>
			<input type="text" class="form-control" required name="adress" placeholder="Dirección..." value="{{ old('adress') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Telefono</label>
			<input type="number" class="form-control" required name="phone" placeholder="Teléfono..." value="{{ old('phone') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Telefono 2</label>
			<input type="number" class="form-control" name="phone2" placeholder="Teléfono..." value="{{ old('phone2') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Correo Electrónico</label>
			<input type="email" class="form-control" name="email" placeholder="Correo Electrónico..." value="{{ old('email') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>NIT</label>
			<input type="text" class="form-control" name="nit" placeholder="NIT..." value="{{ old('nit') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>DPI</label>
			<input type="text" class="form-control" name="dpi" placeholder="DPI..." value="{{ old('dpi') }}">
		</div>
	</div>

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
			<label>Tipo de Cliente</label>
			<select id="id_client_type" name="id_client_type" class="form-control select2" style="width:100%" required>
				<option value="">- Seleccione -</option>
			</select>
		</div>
	</div>


</div>
<div class="modal-footer">
	<button class="btn btn-default" type="submit"><i class="fa fa-check text-green"></i> Guardar</button>
	<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close text-red"></i> Cerrar</button>
</div>
{!!Form::close()!!}
</div>
</div>
</div>

{{-- modal type --}}
	<div class="modal fade modal-slide-in-right" aria-hidden="true"
	role="dialog" tabindex="-1" id="modal-new-type" data-backdrop="static" data-keyboard="false" >
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h4 class="modal-title">Agregar Tipo de Cliente</h4>
				</div>
				<div class="modal-body" style="min-height:200px; ">
					<div class="col=lg-6 col-sm-6 col-md-6 col-xs-12">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" value="" id="new_type_name" class="form-control" placeholder="Nombre...">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label>Tipo de precio</label>
							<select class="form-control" id="new_type_type_price" required>
								<option value="1">Precio 1</option>
								<option value="2">Precio 2</option>
								<option value="3">Precio 3</option>
							</select>
						</div>
					</div>

					<div class="col=lg-12 col-sm-12 col-md-12 col-xs-12">
						<div class="form-group">
							<label>Descripción</label>
							<input type="text" value="" id="new_type_description" class="form-control" placeholder="Descripción...">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" onclick="validar_type();" id="btn-validar-type" class="btn btn-default"><i class="fa fa-check text-green"></i> Confirmar</button>
				</div>
			</div>
		</div>
	</div>

@push('scripts')
<script>
	enviando = false; //Obligaremos a entrar el if en el primer submit

	function checkSubmit() {
		if (!enviando) {
		enviando= true;
		return true;
		}
		else {
		//Si llega hasta aca significa que pulsaron 2 veces el boton submit
		alert("El formulario ya se esta enviando");
		return false;
		}
	}

	var type_addons='';

	$(function() {
	$('#id_client_type').on('select2:opening', llenarType);
});

function llenarType() {
	var data = {!!  json_encode($type) !!};
	var prov ='<option value= "">- Seleccione -</option>';

	for (var a=0; a<data.length; ++a)
	{
		prov += '<option value= "'+data[a].id_client_type+'">'+data[a].name+'</option>';
	}
	prov+=type_addons;
	$('#id_client_type').html(prov);
}

function agg_type() {
	$('#id_client_type').select2('close');
	$('#btn-validar-type').removeAttr('onclick');
	$('#btn-validar-type').attr('onclick', 'validar_type()');
	$("#modal-new-type").modal("show");
}


function validar_type()
{
	var name=$('#new_type_name').val();
	var description=$('#new_type_description').val();
	var type=$('#new_type_type_price').val();
	if(description=='')
	{
		description='0';
	}
	if(name!=''&&type!='')
	{
		$.get('/new/client_type/'+name+'/'+description+'/'+type, function (data) {
			if(data!='0')
			{
				type_addons+='<option value= "'+data+'">'+name+'</option>';
				llenarType();
				$('#id_client_type').val(data);
				$('#new_type_name').val('');
				$('#new_type_description').val('');
				$("#modal-new-type").modal("hide");
			}
			else{
				alert('Ocurrió un error, intente nuevamente');
			}
		});
	}
	else{
		alert('Ingrese correctamente los datos');
	}
}

</script>
@endpush
