@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Listado de Clientes <a href="" data-target="#modal-create-client" data-toggle="modal"><button class="btn btn-default"><i class="fa fa-plus-circle text-green"></i> Nuevo</button></a></h3>
    </div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>NIT</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Opciones</th>
                </thead>

            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('client.client.create')
@include('client.client.detail')
@push ('scripts')
  <script>
  $(document).ready(function() {

    $('#id_client_type').select2({
		dropdownAutoWidth: true,
        dropdownParent: $('#modal-create-client'),
		language: {
      noResults: function() {
        return '<button type="button" class="btn btn-success" id="no-results-btn" onclick="agg_type()">Agregar Tipo de Cliente</a>';
      },
    },
    escapeMarkup: function(markup) {
      return markup;
    },
  });

    $.fn.dataTable.ext.errMode = 'none';
    $('#table_id').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'ocurrio un error', location.reload() );
    } ).DataTable( {
      "order": [[ 0, 'desc' ]],
      "processing": false,
      "infoFiltered":false,
      "ordering":true,
      "paging":true,
      "stateSave": true,
      "stateDuration": -1,
        "serverSide": true,
        "ajax": "/api/clients",
        "columns":[
          {data:'id_client',searchable: false},
          {data:'name'},
          {data:'nit'},
          {data:'adress'},
          {data:'phone'},
          {data:'btn',searchable: false,"orderable": false},
        ],
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        }

    } );
} );

function abrir_edit(i)
{
  var type=$('#id_client_type_edit'+i).val();
  var data = {!!  json_encode($type) !!};

	var prov ='<option value= "">- Seleccione -</option>';
	for (var a=0; a<data.length; ++a)
	{
    if(type==data[a].id_client_type)
    {
      prov += '<option selected value= "'+data[a].id_client_type+'">'+data[a].name+'</option>';
    }
    else{
      prov += '<option value= "'+data[a].id_client_type+'">'+data[a].name+'</option>';
    }
	}
	prov+=type_addons;
	$('#id_client_type_edit'+i).html(prov);

	$('#id_client_type_edit'+i).select2({
		dropdownAutoWidth: true,
        dropdownParent: $('#modal-edit-'+i),
		language: {
      noResults: function() {
        return '<button type="button" class="btn btn-success" id="no-results-btn" onclick="agg_type_edit('+i+')">Agregar Tipo de Cliente</a>';
      },
    },
    escapeMarkup: function(markup) {
      return markup;
    },
  });
}

function agg_type_edit(i)
{
    $('#id_client_type_edit'+i).select2('close');
    $('#btn-validar-type').removeAttr('onclick');
	  $('#btn-validar-type').attr('onclick', 'validar_type_edit('+i+')');
	  $("#modal-new-type").modal("show");
}

function validar_type_edit(i)
{
	var name=$('#new_type_name').val();
	var description=$('#new_type_description').val();
  var type=$('#new_type_type_price').val();
	if(description=='')
	{
		description='0';
	}
	if(name!=''&&type!='')
	{
		$.get('/new/client_type/'+name+'/'+description+'/'+type, function (data) {
			if(data!='0')
			{
				type_addons+='<option value= "'+data+'">'+name+'</option>';
				$('#id_client_type_edit'+i).append('<option value= "'+data+'">'+name+'</option>');
				$('#id_client_type_edit'+i).val(data);
				$("#modal-new-type").modal("hide");
				$('#new_type_name').val('');
        $('#new_type_description').val('');
			}
			else{
				alert('Ocurrió un error, intente nuevamente');
			}
		});
	}
	else{
		alert('Ingrese correctamente los datos');
	}
}
</script>
@endpush
@endsection
