@extends ('layouts.admin')
@section ('contenido')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Listado de Tipos de Clientes <a href="" data-target="#modal-create-client_type" data-toggle="modal"><button class="btn btn-default"><i class="fa fa-plus-circle text-green"></i> Nuevo</button></a></h3>
    </div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" width="100%" id='table_id'>
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Tipo de Precio</th>
                    <th>Opciones</th>
                </thead>

            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include('client.client_type.create')
@push ('scripts')
  <script>
  $(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    $('#table_id').on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'ocurrio un error', location.reload() );
    } ).DataTable( {
      "order": [[ 0, 'desc' ]],
      "processing": false,
      "infoFiltered":false,
      "ordering":true,
      "paging":true,
      "stateSave": true,
      "stateDuration": -1,
        "serverSide": true,
        "ajax": "/api/client_types",
        "columns":[
          {data:'id_client_type',searchable: false},
          {data:'name'},
          {data:'description'},
          {data:'type_price'},
          {data:'btn',searchable: false,"orderable": false},
        ],
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        }

    } );
} );

function abrir_edit(i)
{
}
</script>
@endpush
@endsection
