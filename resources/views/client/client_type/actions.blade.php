<a href="" data-target="#modal-edit-{{$id_client_type}}" onclick="abrir_edit({{$id_client_type}})" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-edit text-blue"></i> Editar</button></a>
@include('client.client_type.edit')
<a href="" data-target="#modal-delete-{{$id_client_type}}" data-toggle="modal"><button class="btn btn-sm btn-default"><i class="fa fa-trash text-red"></i> Anular</button></a>
@include('client.client_type.modal')
