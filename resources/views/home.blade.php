@extends ('layouts.admin')
@section ('contenido')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="container-fluid bg-primary"  style="text-align:center; margin-bottom:20px;">
        <h4 >Bienvenido</h4>
        <h4 class="bg-primary"><i class="fa fa-lg fa-user"></i> <strong>{{auth()->user()->name}}</strong></h4>
        <br>
      </div>
    </div>

    @if($permiso_item->contador == '1')
    <a href="{{url('item/item')}}">
      <div class="card col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="container-fluid bg-red"  style="text-align:center;margin-bottom:20px;">
          <h4>Articulos y Productos</h4>
          <h4 class="bg-red"><i class="fa fa-lg fa-list"></i></h4>
          <br>
        </div>
      </div>
    </a>
    @endif

    <a href="{{url('ventas/ventas')}}">
      <div class="card col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="container-fluid bg-yellow"  style="text-align:center;margin-bottom:20px;">
          <h4>Ventas</h4>
          <h4 class="bg-yellow"><i class="fa fa-lg fa-shopping-cart"></i></h4>
          <br>
        </div>
      </div>
    </a>

    <a href="{{url('ventas/caja')}}">
      <div class="card col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="container-fluid bg-green"  style="text-align:center;margin-bottom:20px;">
          <h4>Venta Nueva</h4>
          <h4 class="bg-green"><i class="fa fa-lg fa-shopping-cart"></i></h4>
          <br>
        </div>
      </div>
    </a>

  </div>
</div>

<br>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

  </div>
</div>

@endsection
