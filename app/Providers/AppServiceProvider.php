<?php

namespace sisVentas\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) 
        {
            if(auth()->user())
            {
                $menu_1 = DB::table('module_rol as r')
                ->join('module as m', 'm.id_module', '=', 'r.id_module')
                ->select('m.id_module', 'm.name', 'm.type', 'm.url', 'm.id_super', 'm.icon', 'm.ordered','r.id_module_rol')
                ->where('r.id_rol', auth()->user()->id_rol)
                ->where('r.permit', '1')
                ->where('m.status', 'ACTIVO')
                ->where('r.status', 'ACTIVO')
                ->where('m.type', '1')
                ->orderBy('m.ordered', 'ASC')
                ->get();
    
                $menu_2 = DB::table('module_rol as r')
                ->join('module as m', 'm.id_module', '=', 'r.id_module')
                ->select('m.id_module', 'm.name', 'm.type', 'm.url', 'm.id_super', 'm.icon', 'm.ordered','r.id_module_rol')
                ->where('r.id_rol', auth()->user()->id_rol)
                ->where('r.permit', '1')
                ->where('m.status', 'ACTIVO')
                ->where('r.status', 'ACTIVO')
                ->where('m.type', '2')
                ->orderBy('m.ordered', 'ASC')
                ->get();
    
                $menu_global='';
                foreach($menu_1 as $m)
                {
                    $menu_global=$menu_global.'<li class="treeview"><a href="/'.$m->url.'"><i class="fa fa-'.$m->icon.'"></i> <span>'.$m->name.'</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
    
                    foreach($menu_2 as $n)
                    {
                        if($n->id_super==$m->id_module)
                        {
                            $menu_global = $menu_global.'<li><a href="/'.$n->url.'"><i class="fa fa-'.$n->icon.'"></i> '.$n->name.'</a></li>';
                        }
                    }
    
                    $menu_global=$menu_global.'</ul></li>';
                }
                $view->with('menu_global', $menu_global);  
            }
        }); 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
