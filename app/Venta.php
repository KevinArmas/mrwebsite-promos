<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = 'venta';

    protected $primaryKey = 'id_venta';

    public $timestamps = false ;

    protected $fillable = [
        'id_user',
        'total',
        'status'
    ];

    protected $guarded = [
        
    ];
}
