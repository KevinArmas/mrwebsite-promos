<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';

    protected $primaryKey = 'id_item';

    public $timestamps = false ;

    protected $fillable = [
        'id_category',
        'name',
        'price',
        'observation',
        'img',
        'status'
    ];

    protected $guarded = [
        
    ];
}
