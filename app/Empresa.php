<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $primaryKey = 'id_empresa';

    public $timestamps = false ;

    protected $fillable = [
        'nombre',
        'nombre_comercial',
        'nit',
        'direccion',
        'telefono',
        'correo',
        'num_cliente',
        'serie',
        'logo',
        'moneda',
        'iva',
        'not_machine',
        'not_camera'
    ];

    protected $guarded = [

    ];
}
