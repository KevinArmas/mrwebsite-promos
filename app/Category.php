<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $primaryKey = 'id_category';

    public $timestamps = false ;

    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    protected $guarded = [
        
    ];
}
