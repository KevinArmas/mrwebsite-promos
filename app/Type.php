<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'type';

    protected $primaryKey = 'id_type';

    public $timestamps = false ;

    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    protected $guarded = [
        
    ];
}
