<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    protected $table = 'client_type';

    protected $primaryKey = 'id_client_type';

    public $timestamps = false ;

    protected $fillable = [
        'name',
        'description',
        'type_price',
        'status'
    ];

    protected $guarded = [
        
    ];
}
