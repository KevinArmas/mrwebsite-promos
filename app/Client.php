<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';

    protected $primaryKey = 'id_client';

    public $timestamps = false ;

    protected $fillable = [
        'name',
        'adress',
        'phone',
        'email',
        'nit',
        'dpi',
        'status',
        'phone2',
        'id_client_type'
    ];

    protected $guarded = [
        
    ];
}
