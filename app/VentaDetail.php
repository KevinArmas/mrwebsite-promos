<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class VentaDetail extends Model
{
    protected $table = 'venta_detail';

    protected $primaryKey = 'id_venta_detail';

    public $timestamps = false ;

    protected $fillable = [
        'id_venta',
        'id_item',
        'sale_price',
        'status'
    ];

    protected $guarded = [
        
    ];
}
