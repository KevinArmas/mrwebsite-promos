<?php

namespace sisVentas\Http\Middleware;

use Closure;

class Soporte
{

    public function handle($request, Closure $next)
    {
      if (! auth()->check()) {
        return redirect('login');
      }

      if (auth()->user()->id_rol!='2'&&auth()->user()->id_rol!='1')
      {
      return redirect('/home');
      }
        return $next($request);
    }
}
