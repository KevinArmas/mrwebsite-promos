<?php

namespace sisVentas\Http\Requests;

use sisVentas\Http\Requests\Request;

class EmpresaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required | max:200',
            'nombre_comercial' => 'required | max:200',
            'nit' => 'required | max:15',
            'direccion' => 'required | max:100',
            'telefono' => 'required | max:20',
            'num_cliente' => 'required',
            'serie' => 'required | max:50',
            'moneda' => 'required | max:2',
            'iva' => 'required',
        ];
    }
}
