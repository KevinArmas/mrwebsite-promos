<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\ClientType;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\ClientTypeFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ClientTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->whereIn('m.id_module',['6', '5'])
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      $permiso = DB::table('module_rol as r')
      ->join('module as m', 'm.id_module', '=', 'r.id_module')
      ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
      ->where('r.id_rol', auth()->user()->id_rol)
      ->where('r.permit', '1')
      ->where('m.id_module','5')
      ->where('m.status', 'ACTIVO')
      ->where('r.status', 'ACTIVO')
      ->orderBy('m.ordered', 'ASC')
      ->first();

      if($permiso->contador=='0')
      {
        Redirect::to('/home')->send();
      }

         return view('client.client_type.index');
    }
    public function listado(Request $request)
    {
        $query = DB::table('client_type')
        ->where('status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','client.client_type.actions')->make(true);
    }

    public function create()
    {
      return view("client.client_type.create");
    }

    public function store (Request $request)
    {
      try{
          DB::beginTransaction();
        $type = new ClientType();
        $type->name = $request->get('name');
        $type->description = $request->get('description');
        $type->type_price = $request->get('type_price');
        $type->status = 'ACTIVO';
        $type->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo tipo de cliente";
        $log->module = "client_type";
        $log->id_elemento = $type->id_client_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('client/client_type');
    }

    public function new ($name, $description, $type_price)
    {
      try{
        DB::beginTransaction();
        $type = new ClientType();
        $type->name = $name;
        if($description=='0')
        {
          $description='';
        }
        $type->description = $description;
        $type->type_price = $type_price;
        $type->status = 'ACTIVO';
        $type->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo tipo de cliente";
        $log->module = "client_type";
        $log->id_elemento = $type->id_client_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();
        
        return $type->id_client_type;
      }
      catch(\Exception $e)
      {
        DB::rollback();

        return 0;
      }
    }

    public function edit($id)
    {
      $type=ClientType::findOrFail($id);

      return view("client.client_type.edit",["type"=>$type]);
    }

    public function update(Request $request)
    {
      try{
          DB::beginTransaction();
          $id = $request->get('id');
          $type = ClientType::findOrFail($id);
          $type->name = $request->get('name');
          $type->description = $request->get('description');
          $type->type_price = $request->get('type_price');
          $type->update();


        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo tipo de cliente";
        $log->module = "client_type";
        $log->id_elemento = $type->id_client_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('client/client_type');
    }



    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $type = ClientType::findOrFail($id);
          $type->status = 'ANULADO';
          $type->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo tipo de cliente";
        $log->module = "client_type";
        $log->id_elemento = $type->id_client_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('client/client_type');
    }
}
