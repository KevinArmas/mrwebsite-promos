<?php

namespace sisVentas\Http\Controllers;


use Illuminate\Support\Facades\Redirect;

use Validator;
use sisVentas\User;
use DB;
use Response;
use Illuminate\Support\Facades\Auth;

class SalirController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function salir()
    {
      Auth::logout();
      return Redirect::to('/');
    }
}
