<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Bitacora_log;
use Illuminate\Support\Facades\Redirect;
use DB;
use Datatables;
use Carbon\Carbon;

class Bitacora_logController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','22')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }
    //este es el index
    public function index()
    {
         return view('security.log.index');
    }
    public function listado(Request $request)
    {
        $query = DB::table('bitacora_log')
        ->join('users', 'bitacora_log.id_user', '=', 'users.id')
        ->select('bitacora_log.id_bitacora_log', 'users.name', 'bitacora_log.acction', 'bitacora_log.module', 'bitacora_log.id_elemento', 'bitacora_log.date');

            return Datatables::queryBuilder($query) ->make(true);
    }

    public function destroy($id)
    {
      try
      {
        DB::beginTransaction();

        $tiempo2=Carbon::now('America/Guatemala')->subMonths(6);
        $borrar = DB::table('bitacora_log')
        ->where('date','<', $tiempo2)
        ->delete();

        DB::commit();
      }
      catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('security/log');
    }
}
