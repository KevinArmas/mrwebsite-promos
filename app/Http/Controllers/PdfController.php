<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Client;
use DB;
use PDF;
use Carbon\Carbon;

class PdfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function pdfcase_machine($inicio, $final, $status, $c_name, $u_name, $tecnico, $n_suc)
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      // sucursal
      if($n_suc != "0")
      {
        // filtro tecnico
        if($tecnico != "0")
        {
          // filtro fecha
          if($inicio != "0" && $final != "0")
          {
            // filtro estado
            if($status != "0")
            {
              // filtro cliente
              if($c_name != "0")
              {
                // filtro usuario
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                // fin filtro usuario
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              // fin filtro cliente
            }
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            // fin filtro estado
          }
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
          // fin filtro fecha
        }
        else
        {
          if($inicio != "0" && $final != "0")
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;
                  
                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users as u', 'u.id', '=', 'c.id_user')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            // else estado
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != "0")
        {
          // filtro fecha, estado, cliente y usuario
          if($inicio != "0" && $final !="0")
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
            // else estado
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
          }
        }
        else
        {
            // filtro fecha, estado, cliente y usuario
          if($inicio != "0" && $final !="0")
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;
                  
                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users as u', 'u.id', '=', 'c.id_user')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 0)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
          }
        }
      }

      // $footer = view('footer');
      // $pdf = PDF::loadView('reports.case_machine.pdf', ["empresa"=>$empresa, "caso"=>$caso])
      // ->setOption('footer-html', $footer)
      // ->setOption('page-size','Letter')
      // ->setOption('margin-bottom', '10mm');

      // return $pdf->stream('case.pdf');

      return view('reports.case_machine.pdf', ["empresa"=>$empresa, "caso"=>$caso]);
    }
    
    public function pdfcase_camera($inicio, $final, $status, $c_name, $u_name, $tecnico, $n_suc)
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      // sucursal
      if($n_suc != "0")
      {
        // filtro tecnico
        if($tecnico != "0")
        {
          // filtro fecha
          if($inicio != "0" && $final !="0")
          {
            // filtro estado
            if($status != "0")
            {
              // filtro cliente
              if($c_name != "0")
              {
                // filtro usuario
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                // fin filtro usuario
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              // fin filtro cliente
            }
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            // fin filtro estado
          }
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
          // fin filtro fecha
        }
        else
        {
          if($inicio != "0" && $final !="0")
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;
                  
                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            // else estado
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != "0")
        {
          // filtro fecha, estado, cliente y usuario
          if($inicio != "0" && $final !="0")
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
            // else estado
            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
          }
        }
        else
        {
            // filtro fecha, estado, cliente y usuario
          if($inicio != "0" && $final !="0")
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;
                  
                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != "0")
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != "0")
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != "0")
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case_camera as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 0)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
          }
        }
      }

      // $footer = view('footer');
      // $pdf = PDF::loadView('reports.case_camera.pdf', ["empresa"=>$empresa, "caso"=>$caso])
      // ->setOption('footer-html', $footer)
      // ->setOption('page-size','Letter')
      // ->setOption('margin-bottom', '10mm');

      // return $pdf->stream('case.pdf');

      return view('reports.case_camera.pdf', ["empresa"=>$empresa, "caso"=>$caso]);
    }

    public function pdfnot_machine($inicio, $final)
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      $not_machine=DB::table('empresa')
      ->select('not_machine')
      ->where('id_empresa','1')
      ->first();

      $mytime = Carbon::now('America/Guatemala');

      $meses_m = $mytime->subMonths($not_machine -> not_machine);

      $date_m = $meses_m->toDateString();

      if($inicio != '0' && $final !='0')
      {
        $n_machine=DB::table('notification_machine as not')
        ->join('machine as ma', 'ma.id_machine', '=', 'not.id_machine')
        ->join('brand as b', 'b.id_brand', '=', 'ma.id_brand')
        ->join('type as t', 't.id_type', '=', 'ma.id_type')
        ->join('case as c', 'c.id_case', '=', 'not.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('not.id_notification', 'not.id_case_initial', 'b.name as brand', 'ma.model', 'not.initial_date', 'cl.name as nameclient', 'not.status')
        ->whereIn('not.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('not.initial_date', '<=', $date_m)
        ->where(DB::raw('SUBSTRING(not.initial_date,1,10)'), '>=', $inicio)
        ->where(DB::raw('SUBSTRING(not.initial_date,1,10)'), '<=', $final)
        ->orderBy('not.initial_date', 'desc')
        ->get();
      }
      else
      {
        $n_machine=DB::table('notification_machine as not')
        ->join('machine as ma', 'ma.id_machine', '=', 'not.id_machine')
        ->join('brand as b', 'b.id_brand', '=', 'ma.id_brand')
        ->join('type as t', 't.id_type', '=', 'ma.id_type')
        ->join('case as c', 'c.id_case', '=', 'not.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('not.id_notification', 'not.id_case_initial', 'b.name as brand', 'ma.model', 'not.initial_date', 'cl.name as nameclient', 'not.status')
        ->whereIn('not.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('not.initial_date', '<=', $date_m)
        ->orderBy('not.initial_date', 'desc')
        ->get();
      }

      $footer = view('footer');
      $pdf = PDF::loadView('reports.services_machine.pdf', ["empresa"=>$empresa, "n_machine"=>$n_machine])
      ->setOption('footer-html', $footer)
      ->setOption('page-size','Letter')
      ->setOption('margin-bottom', '10mm');

      return $pdf->stream('notification.pdf');
    }

    public function pdfnot_camera($inicio, $final)
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      $not_camera=DB::table('empresa')
      ->select('not_camera')
      ->where('id_empresa','1')
      ->first();

      $mytime = Carbon::now('America/Guatemala');

      $meses_c = $mytime->subMonths($not_camera -> not_camera);

      $date_c = $meses_c->toDateString();

      if($inicio != '0' && $final !='0')
      {
        $n_camera=DB::table('notification_camera as noc')
        ->join('camera as ca', 'ca.id_camera', '=', 'noc.id_camera')
        ->join('case_camera as c', 'c.id_case_camera', '=', 'noc.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('noc.id_notification_camera', 'noc.id_case_initial', 'ca.dvr_serie', 'noc.initial_date', 'cl.name as nameclient', 'noc.status')
        ->whereIn('noc.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('noc.initial_date', '<=', $date_c)
        ->where(DB::raw('SUBSTRING(noc.initial_date,1,10)'), '>=', $inicio)
        ->where(DB::raw('SUBSTRING(noc.initial_date,1,10)'), '<=', $final)
        ->orderBy('noc.initial_date', 'desc')
        ->get();
      }
      else
      {
        $n_camera=DB::table('notification_camera as noc')
        ->join('camera as ca', 'ca.id_camera', '=', 'noc.id_camera')
        ->join('case_camera as c', 'c.id_case_camera', '=', 'noc.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('noc.id_notification_camera', 'noc.id_case_initial', 'ca.dvr_serie', 'noc.initial_date', 'cl.name as nameclient', 'noc.status')
        ->whereIn('noc.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('noc.initial_date', '<=', $date_c)
        ->orderBy('noc.initial_date', 'desc')
        ->get();
      }

      $footer = view('footer');
      $pdf = PDF::loadView('reports.services_camera.pdf', ["empresa"=>$empresa, "n_camera"=>$n_camera])
      ->setOption('footer-html', $footer)
      ->setOption('page-size','Letter')
      ->setOption('margin-bottom', '10mm');

      return $pdf->stream('notification.pdf');
    }

    public function pdfservice_machine($inicio, $final, $c_name, $u_name, $tecnico, $n_suc)
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      if($n_suc != '0')
      {
        if($tecnico != '0')
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
        else
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != '0')
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
        }
        else
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser')
                ->where('c.status', 0)
                ->get();

                $name_client=0;

                $name_user=0;

                $total=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
          }
        }
      }

      // $footer = view('footer');
      // $pdf = PDF::loadView('reports.ventas_machine.pdf', ["empresa"=>$empresa, "caso"=>$caso, "total"=>$total])
      // ->setOption('footer-html', $footer)
      // ->setOption('page-size','Letter')
      // ->setOption('margin-bottom', '10mm');

      // return $pdf->stream('service.pdf');

      return view('reports.ventas_machine.pdf', ["empresa"=>$empresa, "caso"=>$caso, "total"=>$total]);
    }

    public function pdfservice_camera($inicio, $final, $c_name, $u_name, $tecnico, $n_suc)
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      if($n_suc != '0')
      {
        if($tecnico != '0')
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
        else
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != '0')
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
        }
        else
        {
          if($inicio != '0' && $final != '0')
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '0')
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '0')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 0)
                ->get();

                $name_client=0;

                $name_user=0;

                $total=0;

                $name_tec=0;

                $name_suc=0;

              }
            }
          }
        }
      }

      // $footer = view('footer');
      // $pdf = PDF::loadView('reports.ventas_camera.pdf', ["empresa"=>$empresa, "caso"=>$caso, "total"=>$total])
      // ->setOption('footer-html', $footer)
      // ->setOption('page-size','Letter')
      // ->setOption('margin-bottom', '10mm');

      // return $pdf->stream('service.pdf');

      return view('reports.ventas_camera.pdf', ["empresa"=>$empresa, "caso"=>$caso, "total"=>$total]);
    }

    public function pdfitem()
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      $item=DB::table('item as i')
      ->join('category as c', 'c.id_category', '=', 'i.id_category')
      ->select('i.id_item', 'c.name as namecategory', 'i.name as nameitem', 'i.price1', 'i.price2', 'i.price3', 'i.observation', 'i.status')
      ->where('i.status', 'ACTIVO')
      ->get();

      $footer = view('footer');
      $pdf = PDF::loadView('reports.items.pdf', ["empresa"=>$empresa, "item"=>$item])
      ->setOption('footer-html', $footer)
      ->setOption('page-size','Letter')
      ->setOption('margin-bottom', '10mm');

      return $pdf->stream('item.pdf');
    }

}
