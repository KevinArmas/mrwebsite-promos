<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\UserCambioFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class UserCambioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $idUser = Auth::id();
        return view("security.cambio.index",["user"=>User::findOrFail($idUser)]);
    }

    public function update(UserCambioFormRequest $request, $id)
    {
      try{
          DB::beginTransaction();
        $user = User::findOrFail($id);
        $user->password = bcrypt($request->get('password'));
        $user->update();


        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo Contraseña";
        $log->module = " usuario";
        $log->id_elemento = $user->id;
        $mytime = Carbon::now('America/Guatemala');
        $log->fecha = $mytime->toDateTimeString();
        $log->save();
        Session::flash('message','Contraseña cambiada correctamente');
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('security/cambio');
    }

}
