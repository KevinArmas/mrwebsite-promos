<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\UserFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','21')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      $rol=DB::table('rol')
      ->where('status', 'ACTIVO')
      ->get();

      $sucursal=DB::table('sucursal')
      ->where('status', 'ACTIVO')
      ->get();

      return view("security.user.index", ["rol"=>$rol, "sucursal"=>$sucursal]);
    }
    public function listado(Request $request)
    {
        $query = DB::table('users')
        ->join('rol', 'rol.id_rol', '=', 'users.id_rol')
        ->join('sucursal', 'sucursal.id_sucursal', '=', 'users.id_sucursal')
        ->select('users.id', 'users.name', 'users.email', 'rol.name as rol as rol', 'users.id_rol', 'users.id_sucursal', 'sucursal.name as sucursal')
        ->where('users.status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','security.user.actions')->make(true);
    }

    public function create()
    {
      $rol=DB::table('rol')
      ->where('status', 'ACTIVO')
      ->get();

      $sucursal=DB::table('sucursal')
      ->where('status', 'ACTIVO')
      ->get();

      return view("security.user.create", ["rol"=>$rol, "sucursal"=>$sucursal]);
    }

    public function store (UserFormRequest $request)
    {
      try{
          DB::beginTransaction();
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->id_rol = $request->get('id_rol');
        $user->status = 'ACTIVO';
        $user->caja='0';
        $user->id_sucursal=$request->get('id_sucursal');
        $user->save();

        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo Usuario";
        $log->module = "user";
        $log->id_elemento = $user->id;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('security/user');
    }

    public function edit($id)
    {
      $user=User::findOrFail($id);

      $rol=DB::table('rol')
      ->where('status', 'ACTIVO')
      ->get();

      $sucursal=DB::table('sucursal')
      ->where('status', 'ACTIVO')
      ->get();

      return view("security.user.edit",["user"=>$user, "rol"=>$rol, "sucursal"=>$sucursal]);
    }

    public function update(UserFormRequest $request)
    {
      try
      {
        DB::beginTransaction();
        $id = $request->get('id');
        $user = User::findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->id_rol = $request->get('id_rol');
        $user->id_sucursal = $request->get('id_sucursal');
        $user->update();

        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo Usuario";
        $log->module = "user";
        $log->id_elemento = $user->id;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();
      }
      catch(\Exception $e)
      {
        DB::rollback();
      }
      return Redirect::to('security/user');
    }
    
    public function destroy($id)
    {
      try{
          DB::beginTransaction();
      $user = User::findOrFail($id);
      $user->status = 'ANULADO';
      $user->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo Usuario";
        $log->module = "user";
        $log->id_elemento = $id;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('security/user');
    }
}
