<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Type;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\TypeFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class TypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->whereIn('m.id_module',['9', '10'])
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      $permiso = DB::table('module_rol as r')
      ->join('module as m', 'm.id_module', '=', 'r.id_module')
      ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
      ->where('r.id_rol', auth()->user()->id_rol)
      ->where('r.permit', '1')
      ->where('m.id_module','9')
      ->where('m.status', 'ACTIVO')
      ->where('r.status', 'ACTIVO')
      ->orderBy('m.ordered', 'ASC')
      ->first();

      if($permiso->contador=='0')
      {
        Redirect::to('/home')->send();
      }

         return view('machine.type.index');
    }
    public function listado(Request $request)
    {
        $query = DB::table('type')
        ->where('status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','machine.type.actions')->make(true);
    }

    public function create()
    {
      return view("machine.type.create");
    }

    public function store (TypeFormRequest $request)
    {
      try{
          DB::beginTransaction();
        $type = new Type();
        $type->name = $request->get('name');
        $type->description = $request->get('description');
        $type->status = 'ACTIVO';
        $type->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo tipo";
        $log->module = "type";
        $log->id_elemento = $type->id_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('machine/type');
    }

    public function new ($name, $description)
    {
      try{
        DB::beginTransaction();
        $type = new Type();
        $type->name = $name;
        if($description=='0')
        {
          $description='';
        }
        $type->description = $description;
        $type->status = 'ACTIVO';
        $type->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo tipo";
        $log->module = "type";
        $log->id_elemento = $type->id_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();
        
        return $type->id_type;
      }
      catch(\Exception $e)
      {
        DB::rollback();

        return 0;
      }
    }

    public function edit($id)
    {
      $type=Type::findOrFail($id);

      return view("machine.type.edit",["type"=>$type]);
    }

    public function update(TypeFormRequest $request)
    {
      try{
          DB::beginTransaction();
          $id = $request->get('id');
          $type = Type::findOrFail($id);
          $type->name = $request->get('name');
          $type->description = $request->get('description');
          $type->update();


        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo tipo";
        $log->module = "type";
        $log->id_elemento = $type->id_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('machine/type');
    }



    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $type = Type::findOrFail($id);
          $type->status = 'ANULADO';
          $type->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo tipo";
        $log->module = "type";
        $log->id_elemento = $type->id_type;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('machine/type');
    }
}
