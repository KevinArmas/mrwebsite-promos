<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\ClientFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->whereIn('m.id_module',['6', '10', '11'])
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      $permiso = DB::table('module_rol as r')
      ->join('module as m', 'm.id_module', '=', 'r.id_module')
      ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
      ->where('r.id_rol', auth()->user()->id_rol)
      ->where('r.permit', '1')
      ->where('m.id_module','6')
      ->where('m.status', 'ACTIVO')
      ->where('r.status', 'ACTIVO')
      ->orderBy('m.ordered', 'ASC')
      ->first();

      if($permiso->contador=='0')
      {
        Redirect::to('/home')->send();
      }

      $type=DB::table('client_type')->where('status', 'ACTIVO')->get();
         return view('client.client.index', ["type"=>$type]);
    }
    public function listado(Request $request)
    {
        $query = DB::table('client')
        ->join('client_type', 'client_type.id_client_type', '=', 'client.id_client_type')
        ->select('client.id_client', 'client.name', 'client.adress', 'client.phone', 'client.email', 'client.nit', 'client.dpi', 'client.status', 'client.phone2', 'client.id_client_type', 'client_type.name as name_type')
        ->where('client.status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','client.client.actions')->make(true);
    }

    public function create()
    {
      
      return view("client.client.create");
    }

    public function store (ClientFormRequest $request)
    {
      try{
          DB::beginTransaction();
        $client = new Client();
        $client->name = $request->get('name');
        $client->adress = $request->get('adress');
        $client->phone = $request->get('phone');
        $client->phone2 = $request->get('phone2');
        $client->email = $request->get('email');
        $client->nit = $request->get('nit');
        $client->dpi = $request->get('dpi');
        $client->id_client_type = $request->get('id_client_type');
        $client->status = 'ACTIVO';
        $client->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo cliente";
        $log->module = "client";
        $log->id_elemento = $client->id_client;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('client/client');
    }

    public function new ($name, $adress, $phone, $email, $nit, $dpi, $phone2, $id_client_type)
    {
      try{
          DB::beginTransaction();
        $client = new Client();
        $client->name = $name;
        $client->adress = $adress;
        $client->phone = $phone;
        if($email=='0')
        {
          $email='';
        }
        $client->email = $email;
        if($nit=='0')
        {
          $nit='';
        }
        if($nit=='C-F')
        {
          $nit='C/F';
        }
        $client->nit = $nit;
        if($dpi=='0')
        {
          $dpi='';
        }
        if($phone2=='0')
        {
          $phone2='';
        }
        $client->phone2 = $phone2;
        $client->dpi = $dpi;
        $client->id_client_type = $id_client_type;
        $client->status = 'ACTIVO';
        $client->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo cliente";
        $log->module = "client";
        $log->id_elemento = $client->id_client;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();
        $dato=$client->id_client.'_'.$client->adress.'_'.$client->phone;
        return $dato;

      }catch(\Exception $e)
      {
        DB::rollback();
        return 0;
      }
    }

    public function edit($id)
    {
      $client=Client::findOrFail($id);

      return view("client.client.edit",["client"=>$client]);
    }
    

    public function show($id)
    {
      $client=DB::table('client')
      ->join('client_type', 'client_type.id_client_type', '=', 'client.id_client_type')
      ->select('client.id_client', 'client.name', 'client.adress', 'client.phone', 'client.email', 'client.nit', 'client.dpi', 'client.status', 'client.phone2', 'client.id_client_type', 'client_type.name as name_type')
      ->where('client.status', 'ACTIVO')
      ->where('client.id_client', $id)
      ->get();

      return $client;
    }

    public function edit_client($id, $name, $adress, $phone, $email, $nit, $dpi, $phone2, $id_client_type)
    {
      try{
        DB::beginTransaction();
      $client = Client::findOrFail($id);
      $client->name = $name;
      $client->adress = $adress;
      $client->phone = $phone;
      if($email=='0')
      {
        $email='';
      }
      $client->email = $email;
      if($nit=='0')
      {
        $nit='';
      }
      $client->nit = $nit;
      if($dpi=='0')
      {
        $dpi='';
      }
      if($phone2=='0')
        {
          $phone2='';
        }
        $client->phone2 = $phone2;
        $client->dpi = $dpi;
        $client->id_client_type = $id_client_type;
      $client->update();

      $log = new Bitacora_log();
      $idUser = Auth::id();
      $log->id_user = $idUser;
      $log->acction = "Actualizo cliente";
      $log->module = "client";
      $log->id_elemento = $client->id_client;
      $mytime = Carbon::now('America/Guatemala');
      $log->date = $mytime->toDateTimeString();
      $log->save();
      DB::commit();
      return $client->id_client;

    }catch(\Exception $e)
    {
      DB::rollback();
      return 0;
    }
    }

    public function update(ClientFormRequest $request)
    {
      try{
          DB::beginTransaction();
          $id = $request->get('id');
          $client = Client::findOrFail($id);
          $client->name = $request->get('name');
          $client->adress = $request->get('adress');
          $client->phone = $request->get('phone');
          $client->phone2 = $request->get('phone2');
          $client->email = $request->get('email');
          $client->nit = $request->get('nit');
          $client->dpi = $request->get('dpi');
          $client->id_client_type = $request->get('id_client_type');
          $client->update();


        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo cliente";
        $log->module = "client";
        $log->id_elemento = $client->id_client;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('client/client');
    }



    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $client = Client::findOrFail($id);
          $client->status = 'ANULADO';
          $client->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo cliente";
        $log->module = "client";
        $log->id_elemento = $client->id_client;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('client/client');
    }
}
