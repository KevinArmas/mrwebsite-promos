<?php

namespace sisVentas\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use sisVentas\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use sisVentas\User;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();

      $permiso_item = DB::table('module_rol as r')
      ->join('module as m', 'm.id_module', '=', 'r.id_module')
      ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
      ->where('r.id_rol', auth()->user()->id_rol)
      ->where('r.permit', '1')
      ->where('m.id_module','17')
      ->where('m.status', 'ACTIVO')
      ->where('r.status', 'ACTIVO')
      ->orderBy('m.ordered', 'ASC')
      ->first();

        return view('home',
        [
          "permiso_item"=>$permiso_item, 
          "empresa"=>$empresa
        ]);
    }

    public function about()
    {
      return view('about');
    }
}
