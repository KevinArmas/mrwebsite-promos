<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Venta;
use sisVentas\VentaDetail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class VentasController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');

    //     $permiso = DB::table('module_rol as r')
    //     ->join('module as m', 'm.id_module', '=', 'r.id_module')
    //     ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
    //     ->where('r.id_rol', auth()->user()->id_rol)
    //     ->where('r.permit', '1')
    //     ->where('m.id_module','17')
    //     ->where('m.status', 'ACTIVO')
    //     ->where('r.status', 'ACTIVO')
    //     ->orderBy('m.ordered', 'ASC')
    //     ->first();

    //     if($permiso->contador=='0')
    //     {
    //       Redirect::to('/home')->send();
    //     }
    // }

    public function index()
    {
      $venta=DB::table('venta')->where('status', 'ACTIVO')->get();
      return view('ventas.ventas.index', ["venta"=>$venta]);
    }

    public function listado(Request $request)
    {
      $query = DB::table('venta')
      ->join('users', 'users.id', '=', 'venta.id_user')
      ->select('venta.id_venta', 'users.name as user', 'venta.total', 'venta.status');
      
      return Datatables::queryBuilder($query)->addColumn('btn','ventas.ventas.actions')->make(true);
    }

    public function show($id)
    {
      $venta=DB::table('venta')
      ->join('users', 'users.id', '=', 'venta.id_user')
      ->select('venta.id_venta', 'users.name as user', 'venta.total', 'venta.status')
      ->where('venta.id_venta', $id)
      ->get();

      $venta_detail=DB::table('venta_detail')
      ->join('venta', 'venta.id_venta', '=', 'venta_detail.id_venta')
      ->join('item', 'item.id_item', '=', 'venta_detail.id_item')
      ->select('item.name', 'venta_detail.sale_price')
      ->where('venta_detail.id_venta', $id)
      ->get();

      return array ($venta, $venta_detail);
    }


    public function store (Request $request)
    {
    }

    public function update(Request $request)
    {
    }

    public function destroy($id)
    {
      try
      {
        DB::beginTransaction();
        $venta = Venta::findOrFail($id);
        $venta->status = 'ANULADA';
        $venta->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo venta";
        $log->module = "venta";
        $log->id_elemento = $venta->id_venta;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }
      catch(\Exception $e)
      {
        DB::rollback();
      }
      return Redirect::to('ventas/ventas');
    }
}
