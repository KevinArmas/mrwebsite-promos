<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Venta;
use sisVentas\VentaDetail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class CajaController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');

    //     $permiso = DB::table('module_rol as r')
    //     ->join('module as m', 'm.id_module', '=', 'r.id_module')
    //     ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
    //     ->where('r.id_rol', auth()->user()->id_rol)
    //     ->where('r.permit', '1')
    //     ->where('m.id_module','17')
    //     ->where('m.status', 'ACTIVO')
    //     ->where('r.status', 'ACTIVO')
    //     ->orderBy('m.ordered', 'ASC')
    //     ->first();

    //     if($permiso->contador=='0')
    //     {
    //       Redirect::to('/home')->send();
    //     }
    // }

    public function index()
    {
      $category=DB::table('category')->where('status', 'ACTIVO')->get();
      $item=DB::table('item')->where('status', 'ACTIVO')->get();
      $mesas=DB::table('mesa')->get();

      return view('ventas.caja.index', ["category"=>$category, "item"=>$item, "mesas"=>$mesas]);
    }

    public function store (Request $request)
    {
      try
      {
        DB::beginTransaction();

        $id_service=$request->get('id_service');
        $buy_price=$request->get('buy_price');
        $sale_price=$request->get('sale_price');

        $venta=new Venta();
        $venta->id_user=Auth::id();
        $venta->total=$request->get('subtotal');
        $venta->status='ACTIVO';
        $venta->save();

        $id=$venta->id_venta;

        $cont=0;

        if($id_service != NULL)
        {
          while($cont<count($id_service))
          {
            if($id_service[$cont] != "" && $buy_price[$cont] != "" && $sale_price[$cont] != "")
            {
              $detail=new VentaDetail();
              $detail->id_venta=$id;
              $detail->id_item=$id_service[$cont];
              $detail->sale_price=$sale_price[$cont];
              $detail->status='ACTIVO';
              $detail->save();
            }
            $cont++;
          }
        }
        DB::commit();
        Session::flash('message', "Venta Realizada con exito!!");
      }
      catch(\Exception $e)
      {
        DB::rollback();
      }
      return Redirect::to('ventas/caja');
    }

    public function update(Request $request)
    {
    }

    public function destroy($id)
    {
    }
}
