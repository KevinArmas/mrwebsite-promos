<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use sisVentas\Http\Requests;
use sisVentas\Item;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\ItemFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','17')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      $category=DB::table('category')->where('status', 'ACTIVO')->get();
      $item=DB::table('item')->where('status', 'ACTIVO')->get();
      return view("item.item.index", ["item"=>$item,"category"=>$category]);
    }
    public function listado(Request $request)
    {
        $query = DB::table('item')
        ->join('category', 'category.id_category', '=', 'item.id_category')
        ->select('item.id_item', 'category.name as category', 'item.id_category', 'item.name', 'item.price', 'item.img', 'item.observation')
        ->where('item.status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','item.item.actions')->make(true);
    }

    public function create()
    {
      $category=DB::table('category')->where('status', 'ACTIVO')->get();
      return view("item.item.create", ["category"=>$category]);
    }

    public function store (ItemFormRequest $request)
    {
      try
      {
          DB::beginTransaction();
          $item = new Item();
          $item->id_category= $request->get('id_category');
          $item->name = $request->get('name');
          $item->price = $request->get('price');
          $item->observation = $request->get('observation');
          $item->status = 'ACTIVO';

          if(Input::hasFile('image'))
          {
            $file=Input::file('image');
            $nombre = $request->get('name').'.'.$file->getClientOriginalExtension();
            $destino = public_path().'/img/productos';
            $file->move($destino, $nombre);
          }

          $item->img = $nombre;

        $item->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo item";
        $log->module = "item";
        $log->id_elemento = $item->id_item;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('item/item');
    }

    public function new ($name, $price, $img, $observation, $id_category)
    {
      try{
          DB::beginTransaction();
        $item = new Item();
        $item->name = $name;
        $item->price = $price;
        if($observation=='0')
        {
          $observation='';
        }
        $item->observation = $observation;
        $item->id_category = $id_category;
        $item->status = 'ACTIVO';
        $item->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo servicio";
        $log->module = "item";
        $log->id_elemento = $item->id_item;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();
        $dato=$item->id_item.'_'.$item->price;
        return $dato;

      }catch(\Exception $e)
      {
        DB::rollback();
        return 0;
      }
    }

    public function edit($id)
    {
      $item=Item::findOrFail($id);
      $category=DB::table('category')->where('status', 'ACTIVO')->get();
      return view("item.item.edit",["item"=>$item,"category"=>$category]);
    }

    public function update(Request $request)
    {
      try{
          DB::beginTransaction();
          $id = $request->get('id');
          $item = Item::findOrFail($id);
          $item->id_category= $request->get('id_category');
          $item->name = $request->get('name');
          $item->price = $request->get('price');
          $item->observation = $request->get('observation');

          if(Input::hasFile('image'))
          {
            $file=Input::file('image');
            $nombre = $request->get('name').'.'.$file->getClientOriginalExtension();
            $destino = public_path().'/img/productos';
            $file->move($destino, $nombre);
            $item->img = $nombre;
          }
          else
          {
            $item->img = $item->img;
          }
          $item->update();

        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo item";
        $log->module = "item";
        $log->id_elemento = $item->id_item;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('item/item');
    }

    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $item = Item::findOrFail($id);
          $item->status = 'ANULADO';
          $item->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo item";
        $log->module = "item";
        $log->id_elemento = $item->id_item;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('item/item');
    }
}
