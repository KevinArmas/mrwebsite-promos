<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Mesa;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\CategoryFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class MesaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','16')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      return view('item.mesa.index');
    }

    public function listado(Request $request)
    {
      $query = DB::table('mesa');
      return Datatables::queryBuilder($query)->addColumn('btn','item.mesa.actions')->make(true);
    }

    public function create()
    {
      return view("item.mesa.create");
    }

    public function store (CategoryFormRequest $request)
    {
      try{
          DB::beginTransaction();
        $category = new Category();
        $category->name = $request->get('name');
        $category->description = $request->get('description');
        $category->status = 'ACTIVO';
        $category->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo categoría";
        $log->module = "category";
        $log->id_elemento = $category->id_category;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('item/category');
    }

    public function edit($id)
    {
      $mesa=Mesa::findOrFail($id);

      return view("item.mesa.edit",["mesa"=>$mesa]);
    }

    public function update(CategoryFormRequest $request)
    {
      try{
          DB::beginTransaction();
          $id = $request->get('id');
          $category = Category::findOrFail($id);
          $category->name = $request->get('name');
          $category->description = $request->get('description');
          $category->update();


        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo categoría";
        $log->module = "category";
        $log->id_elemento = $category->id_category;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('item/category');
    }



    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $category = Category::findOrFail($id);
          $category->status = 'ANULADO';
          $category->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo categoría";
        $log->module = "category";
        $log->id_elemento = $category->id_category;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('item/category');
    }
}
