<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Sucursal;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\SucursalFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class SucursalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','19')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
         return view('security.sucursal.index');
    }
    public function listado(Request $request)
    {
        $query = DB::table('sucursal')
        ->where('status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','security.sucursal.actions')->make(true);
    }

    public function create()
    {
      return view("security.sucursal.create");
    }

    public function store (SucursalFormRequest $request)
    {
      try{
          DB::beginTransaction();
        $sucursal = new Sucursal();
        $sucursal->name = $request->get('name');
        $sucursal->adress = $request->get('adress');
        $sucursal->description = $request->get('description');
        $sucursal->status = 'ACTIVO';
        $sucursal->save();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo Sucursal";
        $log->module = "sucursal";
        $log->id_elemento = $sucursal->id_sucursal;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('security/sucursal');
    }

    public function edit($id)
    {
      $sucursal=Sucursal::findOrFail($id);

      return view("security.sucursal.edit",["sucursal"=>$sucursal]);
    }

    public function update(SucursalFormRequest $request)
    {
      try{
          DB::beginTransaction();
          $id = $request->get('id');
          $sucursal = Sucursal::findOrFail($id);
          $sucursal->name = $request->get('name');
          $sucursal->adress = $request->get('adress');
          $sucursal->description = $request->get('description');
          $sucursal->update();


        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo Sucursal";
        $log->module = "sucursal";
        $log->id_elemento = $sucursal->id_sucursal;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('security/sucursal');
    }



    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $sucursal = Sucursal::findOrFail($id);
          $sucursal->status = 'ANULADO';
          $sucursal->update();

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo Sucursal";
        $log->module = "sucursal";
        $log->id_elemento = $sucursal->id_sucursal;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('security/sucursal');
    }
}
