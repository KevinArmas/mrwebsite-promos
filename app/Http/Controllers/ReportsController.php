<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Machine;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    public function __construct()
    {

    }

    public function case_machine(Request $request)
    {

      $inicio = trim($request->get('inicio'));
      $final = trim($request->get('final'));
      $status = trim($request->get('status'));
      $c_name = trim($request->get('c_name'));
      $u_name = trim($request->get('u_name'));
      $tecnico = trim($request->get('tecnico'));
      $n_suc = trim($request->get('n_suc'));

      $client=DB::table('client')->where('status', 'ACTIVO')->get();
      $user=DB::table('users')->where('status', 'ACTIVO')->get();
      $sucursal=DB::table('sucursal')->where('status', 'ACTIVO')->get();

      // sucursal
      if($n_suc != '')
      {
        // filtro tecnico
        if($tecnico != '')
        {
          // filtro fecha
          if($inicio != '' && $final !='')
          {
            // filtro estado
            if($status != '')
            {
              // filtro cliente
              if($c_name != '')
              {
                // filtro usuario
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                // fin filtro usuario
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              // fin filtro cliente
            }
            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            // fin filtro estado
          }
          else
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.asigned', $tecnico)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
          // fin filtro fecha
        }
        else
        {
          if($inicio != '' && $final !='')
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;
                  
                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users as u', 'u.id', '=', 'c.id_user')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
            // else estado
            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_sucursal', $n_suc)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=DB::table('sucursal')
                  ->where('id_sucursal', $n_suc)
                  ->where('status', 'ACTIVO')
                  ->first();
                }
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != '')
        {
          // filtro fecha, estado, cliente y usuario
          if($inicio != '' && $final !='')
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
            // else estado
            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.asigned', $tecnico)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=DB::table('users')
                  ->where('id', $tecnico)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_suc=0;
                }
              }
            }
          }
        }
        else
        {
            // filtro fecha, estado, cliente y usuario
          if($inicio != '' && $final !='')
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;
                  
                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users as u', 'u.id', '=', 'c.id_user')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                  ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
          }
          // else fecha
          else
          {
            if($status != '')
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.case_status', $status)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
            // else estado

            else
            {
              if($c_name != '')
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_client', $c_name)
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_client', $c_name)
                  ->get();
                      
                  $name_client=DB::table('client')
                  ->select('name', 'phone')
                  ->where('id_client', $c_name)
                  ->where('status', 'ACTIVO')
                  ->first();
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
              else
              {
                if($u_name != '')
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 'ACTIVO')
                  ->where('c.id_user', $u_name)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=DB::table('users')
                  ->where('id', $u_name)
                  ->where('status', 'ACTIVO')
                  ->first();

                  $name_tec=0;

                  $name_suc=0;
                }
                else
                {
                  $caso=DB::table('case as c')
                  ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                  ->join('users', 'users.id', '=', 'c.id_user')
                  ->join('users as u', 'u.id', '=', 'c.asigned')
                  ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                  ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                  ->where('c.status', 0)
                  ->get();
                      
                  $name_client=0;
                      
                  $name_user=0;

                  $name_tec=0;

                  $name_suc=0;
                }
              }
            }
          }
        }
      }

        return view('reports.case_machine.index',
        [
          "client"=>$client,
          "user"=>$user,
          "sucursal"=>$sucursal,
          "caso"=>$caso,
          "inicio"=>$inicio,
          "final"=>$final,
          "status"=>$status,
          "c_name"=>$c_name,
          "u_name"=>$u_name,
          "tecnico"=>$tecnico,
          "n_suc"=>$n_suc,
          "name_client"=>$name_client,
          "name_user"=>$name_user,
          "name_tec"=>$name_tec,
          "name_suc"=>$name_suc
        ]);
      }

      public function case_camera(Request $request)
      {

        $inicio = trim($request->get('inicio'));
        $final = trim($request->get('final'));
        $status = trim($request->get('status'));
        $c_name = trim($request->get('c_name'));
        $u_name = trim($request->get('u_name'));
        $tecnico = trim($request->get('tecnico'));
        $n_suc = trim($request->get('n_suc'));

        $client=DB::table('client')->where('status', 'ACTIVO')->get();
        $user=DB::table('users')->where('status', 'ACTIVO')->get();
        $sucursal=DB::table('sucursal')->where('status', 'ACTIVO')->get();

        // sucursal
        if($n_suc != '')
        {
          // filtro tecnico
          if($tecnico != '')
          {
            // filtro fecha
            if($inicio != '' && $final !='')
            {
              // filtro estado
              if($status != '')
              {
                // filtro cliente
                if($c_name != '')
                {
                  // filtro usuario
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  // fin filtro usuario
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                // fin filtro cliente
              }
              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
              // fin filtro estado
            }
            else
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.asigned', $tecnico)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
            }
            // fin filtro fecha
          }
          else
          {
            if($inicio != '' && $final !='')
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;
                    
                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
            }
            // else fecha
            else
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
              // else estado
              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_sucursal', $n_suc)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=DB::table('sucursal')
                    ->where('id_sucursal', $n_suc)
                    ->where('status', 'ACTIVO')
                    ->first();
                  }
                }
              }
            }
          }
        }
        else
        {
          if($tecnico != '')
          {
            // filtro fecha, estado, cliente y usuario
            if($inicio != '' && $final !='')
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
              }
              // else estado
              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
              }
            }
            // else fecha
            else
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
              }
              // else estado

              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.asigned', $tecnico)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=DB::table('users')
                    ->where('id', $tecnico)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_suc=0;
                  }
                }
              }
            }
          }
          else
          {
              // filtro fecha, estado, cliente y usuario
            if($inicio != '' && $final !='')
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;
                    
                    $name_tec=0;

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
              }
              // else estado

              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                    ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
              }
            }
            // else fecha
            else
            {
              if($status != '')
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.case_status', $status)
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.case_status', $status)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.case_status', $status)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
              }
              // else estado

              else
              {
                if($c_name != '')
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_client', $c_name)
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_client', $c_name)
                    ->get();
                        
                    $name_client=DB::table('client')
                    ->select('name', 'phone')
                    ->where('id_client', $c_name)
                    ->where('status', 'ACTIVO')
                    ->first();
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
                else
                {
                  if($u_name != '')
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 'ACTIVO')
                    ->where('c.id_user', $u_name)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=DB::table('users')
                    ->where('id', $u_name)
                    ->where('status', 'ACTIVO')
                    ->first();

                    $name_tec=0;

                    $name_suc=0;
                  }
                  else
                  {
                    $caso=DB::table('case_camera as c')
                    ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                    ->join('users', 'users.id', '=', 'c.id_user')
                    ->join('users as u', 'u.id', '=', 'c.asigned')
                    ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                    ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname')
                    ->where('c.status', 0)
                    ->get();
                        
                    $name_client=0;
                        
                    $name_user=0;

                    $name_tec=0;

                    $name_suc=0;
                  }
                }
              }
            }
          }
        }

      return view('reports.case_camera.index',
      [
          "client"=>$client,
          "user"=>$user,
          "sucursal"=>$sucursal,
          "caso"=>$caso,
          "inicio"=>$inicio,
          "final"=>$final,
          "status"=>$status,
          "c_name"=>$c_name,
          "u_name"=>$u_name,
          "tecnico"=>$tecnico,
          "n_suc"=>$n_suc,
          "name_client"=>$name_client,
          "name_user"=>$name_user,
          "name_tec"=>$name_tec,
          "name_suc"=>$name_suc
      ]);
    }

    public function not_machine(Request $request)
    {
      $inicio = trim($request->get('inicio'));
      $final = trim($request->get('final'));

      $not_machine=DB::table('empresa')
      ->select('not_machine')
      ->where('id_empresa','1')
      ->first();

      $mytime = Carbon::now('America/Guatemala');

      $meses_m = $mytime->subMonths($not_machine -> not_machine);

      $date_m = $meses_m->toDateString();

      if($inicio != '' && $final !='')
      {
        $n_machine=DB::table('notification_machine as not')
        ->join('machine as ma', 'ma.id_machine', '=', 'not.id_machine')
        ->join('brand as b', 'b.id_brand', '=', 'ma.id_brand')
        ->join('type as t', 't.id_type', '=', 'ma.id_type')
        ->join('case as c', 'c.id_case', '=', 'not.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('not.id_notification', 'not.id_case_initial', 't.name as type', 'b.name as brand', 'ma.model', 'not.initial_date', 'cl.name as nameclient', 'not.status')
        ->whereIn('not.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('not.initial_date', '<=', $date_m)
        ->where(DB::raw('SUBSTRING(not.initial_date,1,10)'), '>=', $inicio)
        ->where(DB::raw('SUBSTRING(not.initial_date,1,10)'), '<=', $final)
        ->orderBy('not.initial_date', 'desc')
        ->get();
      }
      else
      {
        $n_machine=DB::table('notification_machine as not')
        ->join('machine as ma', 'ma.id_machine', '=', 'not.id_machine')
        ->join('brand as b', 'b.id_brand', '=', 'ma.id_brand')
        ->join('type as t', 't.id_type', '=', 'ma.id_type')
        ->join('case as c', 'c.id_case', '=', 'not.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('not.id_notification', 'not.id_case_initial', 't.name as type', 'b.name as brand', 'ma.model', 'not.initial_date', 'cl.name as nameclient', 'not.status')
        ->whereIn('not.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('not.initial_date', '<=', $date_m)
        ->orderBy('not.initial_date', 'desc')
        ->get();
      }
     
      return view('reports.services_machine.index',
      [
        "inicio"=>$inicio,
        "final"=>$final,
        "n_machine"=>$n_machine
      ]);
    }

    public function not_camera(Request $request)
    {
      $inicio = trim($request->get('inicio'));
      $final = trim($request->get('final'));

      $not_camera=DB::table('empresa')
      ->select('not_camera')
      ->where('id_empresa','1')
      ->first();

      $mytime = Carbon::now('America/Guatemala');

      $meses_c = $mytime->subMonths($not_camera -> not_camera);

      $date_c = $meses_c->toDateString();

      if($inicio != '' && $final !='')
      {
        $n_camera=DB::table('notification_camera as noc')
        ->join('camera as ca', 'ca.id_camera', '=', 'noc.id_camera')
        ->join('case_camera as c', 'c.id_case_camera', '=', 'noc.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('noc.id_notification_camera', 'noc.id_case_initial', 'ca.dvr_serie', 'noc.initial_date', 'cl.name as nameclient', 'noc.status')
        ->whereIn('noc.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('noc.initial_date', '<=', $date_c)
        ->where(DB::raw('SUBSTRING(noc.initial_date,1,10)'), '>=', $inicio)
        ->where(DB::raw('SUBSTRING(noc.initial_date,1,10)'), '<=', $final)
        ->orderBy('noc.initial_date', 'desc')
        ->get();
      }
      else
      {
        $n_camera=DB::table('notification_camera as noc')
        ->join('camera as ca', 'ca.id_camera', '=', 'noc.id_camera')
        ->join('case_camera as c', 'c.id_case_camera', '=', 'noc.id_case_initial')
        ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
        ->select('noc.id_notification_camera', 'noc.id_case_initial', 'ca.dvr_serie', 'noc.initial_date', 'cl.name as nameclient', 'noc.status')
        ->whereIn('noc.status', ['SIN ACCION', 'NOTIFICADO'])
        ->where('noc.initial_date', '<=', $date_c)
        ->orderBy('noc.initial_date', 'desc')
        ->get();
      }
     
      return view('reports.services_camera.index',
      [
        "inicio"=>$inicio,
        "final"=>$final,
        "n_camera"=>$n_camera
      ]);
    }

    public function service_machine(Request $request)
    {

      $inicio = trim($request->get('inicio'));
      $final = trim($request->get('final'));
      $c_name = trim($request->get('c_name'));
      $u_name = trim($request->get('u_name'));
      $tecnico = trim($request->get('tecnico'));
      $n_suc = trim($request->get('n_suc'));

      $client=DB::table('client')->where('status', 'ACTIVO')->get();
      $user=DB::table('users')->where('status', 'ACTIVO')->get();
      $sucursal=DB::table('sucursal')->where('status', 'ACTIVO')->get();

      if($n_suc != '')
      {
        if($tecnico != '')
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
        else
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != '')
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
        }
        else
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser')
                ->where('c.status', 0)
                ->get();

                $name_client=0;

                $name_user=0;

                $total=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
          }
        }
      }

      return view('reports.ventas_machine.index',
      [
        "client"=>$client,
        "user"=>$user,
        "sucursal"=>$sucursal,
        "caso"=>$caso,
        "inicio"=>$inicio,
        "final"=>$final,
        "c_name"=>$c_name,
        "u_name"=>$u_name,
        "tecnico"=>$tecnico,
        "n_suc"=>$n_suc,
        "name_client"=>$name_client,
        "name_user"=>$name_user,
        "name_tec"=>$name_tec,
        "name_suc"=>$name_suc,
        "total"=>$total
      ]);
    }

    public function service_camera(Request $request)
    {

      $inicio = trim($request->get('inicio'));
      $final = trim($request->get('final'));
      $c_name = trim($request->get('c_name'));
      $u_name = trim($request->get('u_name'));
      $tecnico = trim($request->get('tecnico'));
      $n_suc = trim($request->get('n_suc'));

      $client=DB::table('client')->where('status', 'ACTIVO')->get();
      $user=DB::table('users')->where('status', 'ACTIVO')->get();
      $sucursal=DB::table('sucursal')->where('status', 'ACTIVO')->get();

      if($n_suc != '')
      {
        if($tecnico != '')
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
        else
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_sucursal', $n_suc)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=0;

                $name_suc=DB::table('sucursal')
                ->where('id_sucursal', $n_suc)
                ->where('status', 'ACTIVO')
                ->first();
              }
            }
          }
        }
      }
      else
      {
        if($tecnico != '')
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('machine_item as mi', 'mi.id_case', '=', 'c.id_case')
                ->select('c.id_case', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_machine_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $name_client=0;

                $name_user=0;

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_tec=DB::table('users')
                ->where('id', $tecnico)
                ->where('status', 'ACTIVO')
                ->first();

                $name_suc=0;
              }
            }
          }
        }
        else
        {
          if($inicio != '' && $final != '')
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.asigned', $tecnico)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '>=', $inicio)
                ->where(DB::raw('SUBSTRING(c.date,1,10)'), '<=', $final)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();

                $name_client=0;

                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
          }
          else
          {
            if($c_name != '')
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_client', $c_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=DB::table('client')
                ->select('name', 'phone')
                ->where('id_client', $c_name)
                ->where('status', 'ACTIVO')
                ->first();
                    
                $name_user=0;

                $name_tec=0;

                $name_suc=0;
              }
            }
            else
            {
              if($u_name != '')
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->groupBy('c.id_case_camera')
                ->get();

                $total=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users as u', 'u.id', '=', 'c.id_user')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'u.name as nameuser',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 'ACTIVO')
                ->where('c.id_user', $u_name)
                ->where('mi.status', '!=', 'ANULADO')
                ->first();
                    
                $name_client=0;
                    
                $name_user=DB::table('users')
                ->where('id', $u_name)
                ->where('status', 'ACTIVO')
                ->first();

                $name_tec=0;

                $name_suc=0;
              }
              else
              {
                $caso=DB::table('case_camera as c')
                ->join('client as cl', 'cl.id_client', '=', 'c.id_client')
                ->join('users', 'users.id', '=', 'c.id_user')
                ->join('users as u', 'u.id', '=', 'c.asigned')
                ->join('camera_item as mi', 'mi.id_case_camera', '=', 'c.id_case_camera')
                ->join('sucursal as su', 'su.id_sucursal', '=', 'c.id_sucursal')
                ->select('c.id_case_camera', 'c.password', 'c.date', 'cl.name as nameclient', 'c.case_status', 'users.name as nameuser', 'u.name as tec', 'su.name as suname',
                DB::raw('COUNT(mi.id_camera_item) as contador'),
                DB::raw('SUM(mi.sale_price) as suma'))
                ->where('c.status', 0)
                ->get();

                $name_client=0;

                $name_user=0;

                $total=0;

                $name_tec=0;

                $name_suc=0;

              }
            }
          }
        }
      }

      return view('reports.ventas_camera.index',
      [
        "client"=>$client,
        "user"=>$user,
        "sucursal"=>$sucursal,
        "caso"=>$caso,
        "inicio"=>$inicio,
        "final"=>$final,
        "c_name"=>$c_name,
        "u_name"=>$u_name,
        "tecnico"=>$tecnico,
        "n_suc"=>$n_suc,
        "name_client"=>$name_client,
        "name_user"=>$name_user,
        "name_tec"=>$name_tec,
        "name_suc"=>$name_suc,
        "total"=>$total
      ]);
    }

    public function item(Request $request)
    {
      $item=DB::table('item as i')
      ->join('category as c', 'c.id_category', '=', 'i.id_category')
      ->select('i.id_item', 'c.name as namecategory', 'i.name as nameitem', 'i.price1', 'i.price2', 'i.price3', 'i.observation', 'i.status')
      ->where('i.status', 'ACTIVO')
      ->get();

      return view('reports.items.index', ["item"=>$item]);
    }
}
