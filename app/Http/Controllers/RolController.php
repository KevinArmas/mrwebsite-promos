<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Rol;
use sisVentas\ModuleRol;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\RolFormRequest;
use sisVentas\Bitacora_log;
use DB;
use Datatables;
use Session;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class RolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','20')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }

    public function index()
    {
      $modules=DB::table('module')
      ->where('status', 'ACTIVO')
      ->get();

      return view('security.rol.index', ["modules"=>$modules]);
    }
    public function listado(Request $request)
    {
        $query = DB::table('rol')
        ->where('status', 'ACTIVO');

            return Datatables::queryBuilder($query)->addColumn('btn','security.rol.actions')->make(true);
    }

    public function create()
    {
      return view("security.rol.create");
    }

    public function store (RolFormRequest $request)
    {
      try{
          DB::beginTransaction();
        $rol = new Rol();
        $rol->name = $request->get('name');
        $rol->description = $request->get('description');
        $rol->status = 'ACTIVO';
        $rol->save();

        $mod=$request->get('mod');
        $permit=$request->get('permit');

        $count=0;
        
        while($count<count($mod))
        {
          $module=new ModuleRol();
          $module->id_rol=$rol->id_rol;
          $module->id_module=$mod[$count];
          $module->permit=$permit[$count];
          $module->status='ACTIVO';
          $module->save();

          $count++;
        }

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Creo rol";
        $log->module = "rol";
        $log->id_elemento = $rol->id_rol;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();
      }

      return Redirect::to('security/rol');
    }

    public function show($id)
    {
      $permisos=DB::select("select m.id_module, m.name, m.id_super, m.type, m.ordered, r.permit from module m left join module_rol r on m.id_module=r.id_module and r.id_rol=$id and r.status='ACTIVO' and m.status='ACTIVO' order by m.ordered asc");

      return $permisos;
    }

    public function update(RolFormRequest $request)
    {
      try{
        DB::beginTransaction();
        $id = $request->get('id');
        $rol = Rol::findOrFail($id);
        $rol->name = $request->get('name');
        $rol->description = $request->get('description');
        $rol->update();

        $mod=$request->get('mod');
        $permit=$request->get('permit');

        $count=0;
        
        while($count<count($mod))
        {
          $module=ModuleRol::firstOrNew(['id_rol' => $id, 'id_module'=>$mod[$count]]);
          $module->id_rol=$rol->id_rol;
          $module->id_module=$mod[$count];
          $module->permit=$permit[$count];
          $module->status='ACTIVO';
          $module->save();

          $count++;
        }

        $log = new Bitacora_log();

        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Actualizo rol";
        $log->module = "rol";
        $log->id_elemento = $rol->id_rol;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('security/rol');
    }



    public function destroy($id)
    {
      try{
          DB::beginTransaction();
          $rol = Rol::findOrFail($id);
          $rol->status = 'ANULADO';
          $rol->update();

          DB::table('module_rol')
          ->where('id_rol', $id)
          ->update(['status'=>'ANULADO']);

        $log = new Bitacora_log();
        $idUser = Auth::id();
        $log->id_user = $idUser;
        $log->acction = "Anulo rol";
        $log->module = "rol";
        $log->id_elemento = $rol->id_rol;
        $mytime = Carbon::now('America/Guatemala');
        $log->date = $mytime->toDateTimeString();
        $log->save();
        DB::commit();

        }catch(\Exception $e)
        {
        DB::rollback();
        }
        return Redirect::to('security/rol');
    }
}
