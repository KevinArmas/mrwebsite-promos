<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Empresa;
use sisVentas\Module;
use Illuminate\Support\Facades\Redirect;
use sisVentas\Http\Requests\EmpresaFormRequest;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Collection;
use DB;
use Datatables;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $permiso = DB::table('module_rol as r')
        ->join('module as m', 'm.id_module', '=', 'r.id_module')
        ->select(DB::raw('COUNT(r.id_module_rol) as contador'))
        ->where('r.id_rol', auth()->user()->id_rol)
        ->where('r.permit', '1')
        ->where('m.id_module','24')
        ->where('m.status', 'ACTIVO')
        ->where('r.status', 'ACTIVO')
        ->orderBy('m.ordered', 'ASC')
        ->first();

        if($permiso->contador=='0')
        {
          Redirect::to('/home')->send();
        }
    }
    public function index()
    {
      $empresa=DB::table('empresa')->where('id_empresa','1')->first();
         return view('configurations.empresa.index', ["empresa"=>$empresa]);
    }

    public function edit($id)
    {
      $empresa=DB::table('empresa')->where('id_empresa',$id)->first();
        return view("configurations.empresa.edit", ["empresa"=>$empresa]);
    }

    public function update(EmpresaFormRequest $request, $id)
    {
      try{
          DB::beginTransaction();
        $empresa = Empresa::findOrFail($id);
        $empresa->nombre = $request->get('nombre');
        $empresa->nombre_comercial = $request->get('nombre_comercial');
        $empresa->nit = $request->get('nit');
        $empresa->direccion = $request->get('direccion');
        $empresa->telefono = $request->get('telefono');
        $empresa->correo = $request->get('correo');
        $empresa->num_cliente = $request->get('num_cliente');
        $empresa->serie = $request->get('serie');
        $empresa->moneda = $request->get('moneda');
        $empresa->iva = $request->get('iva');
        $empresa->not_machine = $request->get('not_machine');
        $empresa->not_camera = $request->get('not_camera');

        if(Input::hasFile('image')){
            $file=Input::file('image');
            $file->move(public_path().'/img/','logo.png');
        }
        $empresa->update();

        DB::commit();

      }catch(\Exception $e)
      {
        DB::rollback();

      }
        return Redirect::to('configurations/empresa');
    }



    public function menu()
    {
      $menu = DB::table('module')
      ->where('status', 'ACTIVO')
      ->where('type', '1')
      ->orderBy('ordered')
      ->get();

      $submenu = DB::table('module')
      ->where('status', 'ACTIVO')
      ->orderBy('ordered')
      ->where('type', '2')
      ->get();

      return view("configurations.empresa.menu", ["menu"=>$menu, "submenu"=>$submenu]);
    }

    public function editar(Request $request)
    {
      $id_module = $request->get('id_module');
      $ordered= $request->get('ordered');

      $count=0;

      while($count<count($id_module))
      {
        $module=Module::findOrFail($id_module[$count]);
        $module->ordered=$ordered[$count];
        $module->update();
        $count++;
      }
      return Redirect::to('menu/empresa');
    }

}
