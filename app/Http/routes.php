<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('layouts/invitado');
    // return view('auth/login');
});

Route::get('soluciones', function () {
  return view('invitado/soluciones');
});

Route::get('precios', function () {
  return view('invitado/precios');
});


Route::get('contacto', function () {
  return view('invitado/contacto');
});

Route::get('recursos', function () {
  return view('invitado/recursos');
});

Route::get('aplicaciones', function () {
  return view('invitado/aplicaciones');
});

Route::group(['middleware'=>'auth'], function()
{
  Route::resource('security/cambio', 'UserCambioController');
  Route::get('salir', 'SalirController@salir');
  Route::get('about', 'HomeController@about');

  Route::resource('client/client', 'ClientController');
  route::get('/api/clients','ClientController@listado');
  route::get('/new/client/{name}/{adress}/{phone}/{email}/{nit}/{dpi}/{phone2}/{id_client_type}','ClientController@new');
  route::get('/edit/client/{id}/{name}/{adress}/{phone}/{email}/{nit}/{dpi}/{phone2}/{id_client_type}','ClientController@edit_client');
  route::post('editar/client','ClientController@update');

  Route::resource('client/client_type', 'ClientTypeController');
  route::get('/api/client_types','ClientTypeController@listado');
  route::get('/new/client_type/{name}/{description}/{type_price}','ClientTypeController@new');
  route::post('editar/client_type','ClientTypeController@update');

  Route::resource('item/category', 'CategoryController');
  route::get('/api/categorys','CategoryController@listado');
  route::post('editar/category','CategoryController@update');

  Route::resource('item/item', 'ItemController');
  route::get('/api/items','ItemController@listado');
  route::get('/new/item/{name}/{price1}/{price2}/{price3}/{img}/{observation}/{id_category}','ItemController@new');
  route::post('editar/item','ItemController@update');

  Route::resource('mesa', 'MesaController');
  route::get('/api/mesas','MesaController@listado');
  route::post('editar/mesa','MesaController@update');

  Route::resource('ventas/ventas', 'VentasController');
  route::get('/api/ventas','VentasController@listado');

  Route::resource('ventas/caja', 'CajaController');

  // reportes

  Route::get('report/item', 'ReportsController@item');
  Route::get('pdfitem', 'PdfController@pdfitem');

  // 

  Route::resource('configurations/empresa', 'EmpresaController');
  Route::resource('menu/empresa', 'EmpresaController@menu');
  route::post('editar/menu','EmpresaController@editar');

  Route::resource('security/user', 'UserController');
  route::get('/api/users','UserController@listado');
  route::post('editar/user','UserController@update');

  Route::resource('security/sucursal', 'SucursalController');
  route::get('/api/sucursales','SucursalController@listado');
  route::post('editar/sucursal','SucursalController@update');

  Route::resource('security/rol', 'RolController');
  route::get('/api/roles','RolController@listado');
  route::post('editar/rol','RolController@update');

  Route::resource('security/log', 'Bitacora_logController');
  route::get('/api/logs','Bitacora_logController@listado');
});

Route::auth();
Route::get('/home', 'HomeController@index');
Route::get('/{slug?}', 'HomeController@index');
