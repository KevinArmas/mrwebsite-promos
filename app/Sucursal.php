<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursal';

    protected $primaryKey = 'id_sucursal';

    public $timestamps = false ;

    protected $fillable = [
        'name',
        'adress',
        'description',
        'status'
    ];

    protected $guarded = [
        
    ];
}
