<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'module';

    protected $primaryKey = 'id_module';

    public $timestamps = false ;

    protected $fillable = [
        'name',
        'type',
        'url',
        'id_super',
        'icon',
        'ordered',
        'status'
    ];

    protected $guarded = [
        
    ];
}
