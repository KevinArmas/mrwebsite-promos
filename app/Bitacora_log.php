<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Bitacora_log extends Model
{
    protected $table = 'bitacora_log';

    protected $primaryKey = 'id_bitacora_log';

    public $timestamps = false ;

    protected $fillable = [
        'id_user',
        'acciton',
        'module',
        'id_elemento',
        'date'
    ];

    protected $guarded = [
        
    ];
}
