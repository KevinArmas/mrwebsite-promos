<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class ModuleRol extends Model
{
    protected $table = 'module_rol';

    protected $primaryKey = 'id_module_rol';

    public $timestamps = false ;

    protected $fillable = [
        'id_rol',
        'id_module',
        'permit',
        'status'
    ];

    protected $guarded = [
        
    ];
}
