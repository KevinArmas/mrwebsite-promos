<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $table = 'mesa';

    protected $primaryKey = 'id_mesa';

    public $timestamps = false ;

    protected $fillable = [
        'numero',
        'image',
        'status'
    ];

    protected $guarded = [
        
    ];
}
